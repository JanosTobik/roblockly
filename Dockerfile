FROM node:12.4-alpine

RUN apk update && apk add git

# RUN git clone https://github.com/ABC-iRobotics/ur-state-receiver.git

# WORKDIR /ur-state-receiver
# RUN npm install
# WORKDIR /

RUN git clone https://JanosTobik@bitbucket.org/JanosTobik/roblockly.git

WORKDIR /roblockly/server
RUN npm install
RUN npm install pm2 -g
WORKDIR /roblockly/client
RUN npm install
RUN npm run build
RUN rm -rf /node_modules
WORKDIR /roblockly


# application
ENV NODE_ENV=production
ENV URL=http://localhost:8080
ENV PORT=8080
ENV TOKEN_SECRET=jwt_secret
ENV SQL_LOGGING=true
ENV HTTP_LOGGING=true

# sqlite database
ENV DB_PATH=./database/RoblocklyDatabase.db
 
# email
ENV SMTP_SERVER=smtp.gmail.com
ENV SMTP_PORT=465
#true for 465, false for other ports
ENV SMTP_SECURED=true
ENV EMAIL_USER=roblocklyweb@gmail.com
ENV EMAIL_PASS=QWErty01

# RRSM
ENV RRSM_URL=http://rrsm:3000

WORKDIR /roblockly/server
CMD ["pm2-runtime", "-n", "Roblockly", "--disable-logs", "start", "app.js"]
EXPOSE 8080