const Project = require('../models/Project');

module.exports = {
    findProjectById: function(projectId) {
        return new Promise((resolve, reject) => {
            Project.findByPk(projectId)
            .then(project => {project ? resolve(project) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    createNewProject: function(userId, projectName, description) {
        return new Promise((resolve, reject) => {
            Project.create({
                projectName: projectName,
                description: description,
                definition: null,
                created: new Date(),
                fk_User_userId: userId,
                comment: ''
            })
            .then(project => {project ? resolve(project) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    saveProject: function(project){
        return new Promise((resolve, reject) => {
            Project.findByPk(project.projectId)
            .then(res => {
                Object.assign(res, project);
                res.modified = new Date();
                res.save({
                    fields: ['projectName', 'description', 'definition', 'modified']
                });
                resolve(res);
            })
            .catch(() => {reject('ERROR');});
        });
    },

    deleteProject: function(projectId){
        return new Promise((resolve, reject) => {
            Project.findByPk(projectId)
            .then(res => {
                res.destroy()
                .then(() => {resolve('SUCCESS');})
                .catch(() => {reject('ERROR');});
            })
            .catch(() => {reject('ERROR');});
        });
    }
}