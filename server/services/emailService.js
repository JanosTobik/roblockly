const nodemailer = require('nodemailer');

// before sending your email using gmail you have to allow non secure apps to access gmail,
// you can enable it in your google account here: https://myaccount.google.com/intro/security
const transport = nodemailer.createTransport({
    host: process.env.SMTP_SERVER,
    port: process.env.SMTP_PORT,
    secure: process.env.SMTP_SECURED,
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

module.exports = {
    sendMail: function (receiver, content) {
        const options = {
            from: process.env.EMAIL_USER,
            to: receiver,
            subject: content.subject,
            text: content.text,
            html: content.html
        };

        transport.sendMail(options, (err, res) => {
            if(err) {
                console.log(err);
            } else {
                console.log(res);
                return res;
            }
        });
    }
}