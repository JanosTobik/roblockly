const cheerio = require('cheerio');

const Tool = require('../models/Tool');
const userService = require('../services/userService');

module.exports = {
    findToolById: function (toolId) {
        return new Promise((resolve, reject) => {
            Tool.findByPk(toolId)
            .then(tool => tool ? resolve(tool) : reject('ERROR'))
            .catch(() => reject('ERROR'));
        });
    },

    createNewTool: function (userId, toolName, description, tcpPose, payloadMass, payloadCog) {
        return new Promise((resolve, reject) => {
            Tool.create({
                toolName: toolName,
                description: description,
                tcpPose: tcpPose,
                payloadMass: payloadMass,
                payloadCog: payloadCog,
                created: new Date(),
                fk_User_userId: userId
            })
            .then(tool => {tool ? resolve(tool) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    deleteTool: function (toolId) {
        return new Promise((resolve, reject) => {
            Tool.findByPk(toolId)
            .then(tool => {
                tool.getUser().then(user => {
                    user.getProjects().then(projects => {
                        projects.forEach(project => {
                            const $ = cheerio.load(project.definition, {
                                xml: {
                                    withDomLvl1: true,
                                    xmlMode: true
                                }
                            });
                            if ($(`[type="${tool.toolName}"]`).length !== 0) {
                                $(`[type="${tool.toolName}"]`).remove();
                                project.definition = $.xml();
                                project.save({
                                    fields: ['definition']
                                });
                            }
                        })
                    })

                    user.getRoutines().then(routines => {
                        routines.forEach(routine => {
                            const $ = cheerio.load(routine.definition, {
                                xml: {
                                    withDomLvl1: true,
                                    xmlMode: true
                                }
                            });
                            if ($(`[type="${tool.toolName}"]`).length !== 0) {
                                $(`[type="${tool.toolName}"]`).remove();
                                routine.definition = $.xml();
                                routine.save({
                                    fields: ['definition']
                                });
                            }
                        })
                    });
                })

                tool.destroy()
                .then(() => resolve('SUCCESS'))
                .catch(() => reject('ERROR'));
            })
            .catch(() => reject('ERROR'));
        });
    },

    saveTool: function(_tool) {
        return new Promise((resolve, reject) => {
            Tool.findByPk(_tool.toolId)
            .then(tool => {
                if (tool['toolName'] !== _tool['toolName']) {
                    const oldName = tool.toolName;
                    const newName = _tool.toolName;

                    tool.getUser().then(user => {
                        user.getProjects().then(projects => {
                            projects.forEach(project => {
                                const $ = cheerio.load(project.definition, {
                                    xml: {
                                        withDomLvl1: true,
                                        xmlMode: true
                                    }
                                });
                                
                                if ($(`[type="${oldName}"]`).length !== 0) {
                                    $(`[type="${oldName}"]`).attr('type', newName);
                                    project.definition = $.xml();
                                    project.save({
                                        fields: ['definition']
                                    });
                                }
                            })
                        });

                        user.getRoutines().then(routines => {
                            routines.forEach(routine => {
                                const $ = cheerio.load(routine.definition, {
                                    xml: {
                                        withDomLvl1: true,
                                        xmlMode: true
                                    }
                                });
    
                                if ($(`[type="${oldName}"]`).length !== 0) {
                                    $(`[type="${oldName}"]`).attr('type', newName);
                                    routine.definition = $.xml();
                                    routine.save({
                                        fields: ['definition']
                                    });
                                }
                            })
                        });
                    })
                }

                Object.assign(tool, _tool);
                tool.modified = new Date();
                tool.save({
                    fields: ['toolName', 'description', 'tcpPose', 'payloadMass', 'payloadCog', 'modified']
                });
                resolve(tool);
            })
            .catch(() => {reject('ERROR');});
        });
    }
}