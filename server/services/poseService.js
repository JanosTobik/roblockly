const cheerio = require('cheerio');

const Pose = require('../models/Pose');
const userService = require('../services/userService');

module.exports = {
    findPoseById: function (poseId) {
        return new Promise((resolve, reject) => {
            Pose.findByPk(poseId)
            .then(pose => pose ? resolve(pose) : reject('ERROR'))
            .catch(() => reject('ERROR'));
        });
    },

    createNewPose: function (userId, poseName, code, poseType, description) {
        return new Promise((resolve, reject) => {
            Pose.create({
                poseName: poseName,
                code: code,
                poseType: poseType,
                description: description,
                created: new Date(),
                fk_User_userId: userId
            })
            .then(pose => {pose ? resolve(pose) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    deletePose: function (poseId) {
        return new Promise((resolve, reject) => {
            Pose.findByPk(poseId)
            .then(pose => {
                pose.getUser().then(user => {
                    user.getProjects().then(projects => {
                        projects.forEach(project => {
                            const $ = cheerio.load(project.definition, {
                                xml: {
                                    withDomLvl1: true,
                                    xmlMode: true
                                }
                            });
                            if ($(`[type="${pose.poseName}"]`).length !== 0) {
                                $(`[type="${pose.poseName}"]`).remove();
                                project.definition = $.xml();
                                project.save({
                                    fields: ['definition']
                                });
                            }
                        })
                    });

                    user.getRoutines().then(routines => {
                        routines.forEach(routine => {
                            const $ = cheerio.load(routine.definition, {
                                xml: {
                                    withDomLvl1: true,
                                    xmlMode: true
                                }
                            });
                            if ($(`[type="${pose.poseName}"]`).length !== 0) {
                                $(`[type="${pose.poseName}"]`).remove();
                                routine.definition = $.xml();
                                routine.save({
                                    fields: ['definition']
                                });
                            }
                        })
                    });
                })

                pose.destroy()
                .then(() => resolve('SUCCESS'))
                .catch(() => reject('ERROR'));
            })
            .catch(() => reject('ERROR'));
        });
    },

    savePose: function(_pose) {
        return new Promise((resolve, reject) => {
            Pose.findByPk(_pose.poseId)
            .then(pose => {
                if (pose['poseName'] !== _pose['poseName']) {
                    const oldName = pose.poseName;
                    const newName = _pose.poseName;
                    
                    pose.getUser().then(user => {
                        user.getProjects().then(projects => {
                            projects.forEach(project => {
                                const $ = cheerio.load(project.definition, {
                                    xml: {
                                        withDomLvl1: true,
                                        xmlMode: true
                                    }
                                });

                                if ($(`[type="${oldName}"]`).length !== 0) {
                                    $(`[type="${oldName}"]`).attr('type', newName);
                                    project.definition = $.xml();
                                    project.save({
                                        fields: ['definition']
                                    });
                                }
                            })
                        });

                        user.getRoutines().then(routines => {
                            routines.forEach(routine => {
                                const $ = cheerio.load(routine.definition, {
                                    xml: {
                                        withDomLvl1: true,
                                        xmlMode: true
                                    }
                                });

                                if ($(`[type="${oldName}"]`).length !== 0) {
                                    $(`[type="${oldName}"]`).attr('type', newName);
                                    routine.definition = $.xml();
                                    routine.save({
                                        fields: ['definition']
                                    });
                                }
                            })
                        });
                    })
                }

                Object.assign(pose, _pose);
                pose.modified = new Date();
                pose.save({
                    fields: ['poseName', 'description', 'modified']
                });
                resolve(pose);
            })
            .catch(() => {reject('ERROR');});
        });
    }
}