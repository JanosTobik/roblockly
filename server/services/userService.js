const User = require('../models/User');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const emailService = require('./emailService');
const emailTemplates = require('../utils/emailTemplates');
const sequelize = require('../config/databaseConfig');

module.exports = {
    findUserById : function(userId) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId, {
                attributes: ['userId', 'email', 'created', 'modified', 'comment']
            })
            .then(user => {user ? resolve(user) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    createNewUser: function(email, password) {
        return new Promise((resolve, reject) => {
            User.create({
                email: email,
                password: password,
                created: new Date(),
                modified: null,
                emailConfirmed: 0,
                confirmationKey: crypto.randomBytes(32).toString('hex')
            })
            .then(user => {
                resolve(user);
            })
            .catch(() => {reject('ERROR');});
        });
    },

    findUserByEmail: function(email) {
        return new Promise((resolve, reject) => {
            User.findOne({
                where: {
                    email: email
                }
            })
            .then(user => {user ? resolve(user) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    findUserProjects: function(userId) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId)
            .then(user => {
                user.getProjects()
                .then(projects => {resolve(projects);})
                .catch(() => {reject('ERROR');})
            })
            .catch(() => {reject('ERROR');})
        });
    },

    findUserRoutines: function(userId) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId)
            .then(user => {
                user.getRoutines()
                .then(routines => {resolve(routines);})
                .catch(() => {reject('ERROR');})
            })
            .catch(() => {reject('ERROR');})
        });
    },

    findUserPoses: function(userId) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId)
            .then(user => {
                user.getPoses()
                .then(poses => {resolve(poses);})
                .catch(() => {reject('ERROR');})
            })
            .catch(() => {reject('ERROR');})
        });
    },

    findUserTools: function(userId) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId)
            .then(user => {
                user.getTools()
                .then(tools => {resolve(tools);})
                .catch(() => {reject('ERROR');})
            })
            .catch(() => {reject('ERROR');})
        });
    },

    changePassword: function(userId, oldPassword, newPassword) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId)
            .then(user => {
                if(!user.validPassword(oldPassword))
                {
                    reject('ERROR');
                } else {
                    const salt = bcrypt.genSaltSync();
                    user.update({ password: bcrypt.hashSync(newPassword, salt), modified: new Date() });
                    resolve(user);
                }
            })
            .catch(() => {reject('ERROR');});
        });
    },

    deleteUser: function(userId) {
        return new Promise((resolve, reject) => {
            User.findByPk(userId)
            .then(user => {
                user.destroy()
                .then(() => {resolve('SUCCESS');})
                .catch(() => {reject('ERROR');});
            })
            .catch(() => {reject('ERROR');});
        });
    },

    confirmEmail: function(key) {
        return new Promise((resolve, reject) => {
            User.findOne({
                where: {
                    confirmationKey: key
                }
            })
            .then(user => {
                if (user) {
                    user.update({ emailConfirmed: 1 });
                    resolve(user);
                } else {
                    reject('ERROR');
                }
            })
            .catch(() => {reject('ERROR');});
        });
    },

    deleteExpiredUsers: function() {
        return new Promise((resolve, reject) => {
            sequelize.query(
                `select * from "User" where DATETIME(created, '+3 day') < CURRENT_TIMESTAMP and emailConfirmed = 0`,
                {
                    model: User,
                    mapToModel: true
                }
            )
            .then(users => {
                users.forEach(user => {
                    user.destroy()
                    .then(deletedUser => {
                        emailService.sendMail(user.email, emailTemplates.expiredTemplate());
                        console.log(`UserId: ${user.userId} has been deleted, e-mail has been sent to the user`)
                        return deletedUser;
                    })
                    .catch(() => console.log(`UserId: ${user.userId} has not been deleted`))
                });
                resolve(users);
            })
            .catch(err => { reject(err); });
        });
    }

}