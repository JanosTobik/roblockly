const cheerio = require('cheerio');

const Routine = require('../models/Routine');
const userService = require('../services/userService');

module.exports = {
    findRoutineById: function(routineId) {
        return new Promise((resolve, reject) => {
            Routine.findByPk(routineId)
            .then(routine => {routine ? resolve(routine) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    createNewRoutine: function(userId, routineName, description) {
        return new Promise((resolve, reject) => {
            Routine.create({
                routineName: routineName,
                description: description,
                definition: null,
                created: new Date(),
                fk_User_userId: userId,
                comment: ''
            })
            .then(routine => {routine ? resolve(routine) : reject('ERROR');})
            .catch(() => {reject('ERROR');});
        });
    },

    saveRoutine: function(_routine){
        return new Promise((resolve, reject) => {
            Routine.findByPk(_routine.routineId)
            .then(routine => {
                if (routine['routineName'] !== _routine['routineName']) {
                    const oldName = routine.routineName;
                    const newName = _routine.routineName;

                    routine.getUser().then(user => {
                        user.getProjects().then(projects => {
                            projects.forEach(project => {
                                const $ = cheerio.load(project.definition, {
                                    xml: {
                                        withDomLvl1: true,
                                        xmlMode: true
                                    }
                                });

                                if ($(`[type="${oldName}"]`).length !== 0) {
                                    $(`[type="${oldName}"]`).attr('type', newName);
                                    project.definition = $.xml();
                                    project.save({
                                        fields: ['definition']
                                    });
                                }
                            })
                        })
                    })
                }

                Object.assign(routine, _routine);
                routine.modified = new Date();
                routine.save({
                    fields: ['routineName', 'description', 'definition', 'modified']
                });
                resolve(routine);
            })
            .catch(() => {reject('ERROR');});
        });
    },

    deleteRoutine: function(routineId){
        return new Promise((resolve, reject) => {
            Routine.findByPk(routineId)
            .then(routine => {
                routine.getUser().then(user => {
                    user.getProjects().then(projects => {
                        projects.forEach(project => {
                            const $ = cheerio.load(project.definition, {
                                xml: {
                                    withDomLvl1: true,
                                    xmlMode: true
                                }
                            });

                            if ($(`[type="${routine.routineName}"]`).length !== 0) {
                                $(`[type="${routine.routineName}"]`).remove();
                                project.definition = $.xml();
                                project.save({
                                    fields: ['definition']
                                });
                            }
                        })
                    })
                })

                routine.destroy()
                .then(() => {resolve('SUCCESS');})
                .catch(() => {reject('ERROR');});
            })
            .catch(() => {reject('ERROR');});
        });
    }
}