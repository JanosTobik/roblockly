const urconnection = require('../utils/URConnection');

module.exports = {
    setupConnection: function() {
        urconnection.connect();
    },

    sendMsgToUR : function(msg) {
        urconnection.sendMessage(msg);
    },

    closeConnection: function() {
        urconnection.closeConnection();
    }
}