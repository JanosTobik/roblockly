var workspace = null;
var code = '';


window.onload = function start() {
    var toolbox = document.getElementById("toolbox");
    var options = {
        toolbox : toolbox,
        trashcan : true,
        scrollbars : true,
        sounds : true,
        grid : {
            spacing : 20,
            length : 1,
            colour : '#ccc',
            snap : false
        },
        zoom : {
            controls : true,
            wheel : false,
            startScale : 1, 
		    maxScale : 3, 
		    minScale : 0.3, 
		    scaleSpeed : 1.2
        }
    };
    workspace = Blockly.inject('blocklyDiv', options);
    workspace.addChangeListener(renderContent);

    var workspaceBlocks = document.getElementById("workspaceBlocks");
        
    Blockly.Xml.domToWorkspace(workspaceBlocks, workspace);
}

function renderContent() {
    var content = document.getElementById('code');
    code = Blockly.UR_script.workspaceToCode(workspace);
    //content.textContent = code;
    
    /* workspace to xml */
    /*
    var xml = Blockly.Xml.workspaceToDom(workspace);
    var xmlText = Blockly.Xml.domToPrettyText(xml);
    */
}

function playCode() {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/robot/program/ur", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        codeToSend: code
    }));
}