/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Google Inc.
 * https://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Procedure blocks for Blockly.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.Blocks.ur_blocks');
goog.provide('Blockly.Constants.ur_blocks');

goog.require('Blockly.Blocks');

Blockly.Blocks['main_block'] = {
    init: function() {
      this.appendDummyInput()
          .appendField(new Blockly.FieldTextInput("MyFunction"), "FunctionName");
      this.appendStatementInput("STACK")
          .setCheck(null);
      this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
    }

};

Blockly.Blocks['movej'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("movej")
          .appendField("q1:")
          .appendField(new Blockly.FieldNumber(-1.95, -Infinity, Infinity, 0), "Q1")
          .appendField("q2:")
          .appendField(new Blockly.FieldNumber(-1.58, -Infinity, Infinity, 0), "Q2")
          .appendField("q3:")
          .appendField(new Blockly.FieldNumber(1.16, -Infinity, Infinity, 0), "Q3")
          .appendField("q4:")
          .appendField(new Blockly.FieldNumber(-1.15, -Infinity, Infinity, 0), "Q4")
          .appendField("q5:")
          .appendField(new Blockly.FieldNumber(-1.55, -Infinity, Infinity, 0), "Q5")
          .appendField("q6:")
          .appendField(new Blockly.FieldNumber(1.18, -Infinity, Infinity, 0), "Q6")
          .appendField("a:")
          .appendField(new Blockly.FieldNumber(0, 1.0, 1, 0), "A")
          .appendField("v:")
          .appendField(new Blockly.FieldNumber(0, 1.0, 1, 0), "V");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(150);
   this.setTooltip("");
   this.setHelpUrl("");
    }
};

Blockly.Blocks['sleep'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("sleep")
          .appendField(new Blockly.FieldNumber(0, 0), "SLEEP");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(193);
   this.setTooltip("");
   this.setHelpUrl("");
    }
};

Blockly.Blocks['position'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("position");
      this.appendValueInput("X")
          .setCheck("Number")
          .appendField("X");
      this.appendValueInput("Y")
          .setCheck("Number")
          .appendField("Y");
      this.appendValueInput("Z")
          .setCheck("Number")
          .appendField("Z");
      this.appendValueInput("RX")
          .setCheck("Number")
          .appendField("RX");
      this.appendValueInput("RY")
          .setCheck("Number")
          .appendField("RZ");
      this.appendValueInput("RZ")
          .setCheck("Number")
          .appendField("RY");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(150);
   this.setTooltip("");
   this.setHelpUrl("");
    }
};

Blockly.Blocks['rotation'] = {
    init: function() {
      this.appendValueInput("Q1")
          .setCheck("Number")
          .appendField("rotate")
          .appendField("q1");
      this.appendValueInput("Q2")
          .setCheck("Number")
          .appendField("q2");
      this.appendValueInput("Q3")
          .setCheck("Number")
          .appendField("q3");
      this.appendValueInput("Q4")
          .setCheck("Number")
          .appendField("q4");
      this.appendValueInput("Q5")
          .setCheck("Number")
          .appendField("q5");
      this.appendValueInput("Q6")
          .setCheck("Number")
          .appendField("q6");
      this.setInputsInline(true);
      this.setOutput(true, "rotation");
      this.setColour(150);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['move_joint'] = {
    init: function() {
      this.appendValueInput("JPOSE")
          .setCheck("joint-pose")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("move-joint")
          .appendField(new Blockly.FieldDropdown([["abs","ABSOLUTE"], ["rel","RELATIVE"]]), "ABS_REL")
          .appendField("jpose");
      this.appendValueInput("ANG_ACCELERATION")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("angular acceleration");
      this.appendValueInput("ANG_VELOCITY")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("angular velocity");
      this.appendValueInput("TIME")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("t");
      this.appendValueInput("RADIUS")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("r");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(193);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['move_cart'] = {
    init: function() {
      this.appendValueInput("CART-POSE")
          .setCheck("cart-pose")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("move-cart")
          .appendField(new Blockly.FieldDropdown([["abs","ABSOLUTE"], ["rel","RELATIVE"]]), "ABS_REL")
          .appendField("cart-pose");
      this.appendValueInput("ACCELERATION")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("acceleration");
      this.appendValueInput("VELOCITY")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("velocity");
      this.appendValueInput("TIME")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("t");
      this.appendValueInput("RADIUS")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("r");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(193);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['cart_pose'] = {
    init: function() {
      this.appendValueInput("X")
          .setCheck("Number")
          .appendField("cart-pose")
          .appendField("x");
      this.appendValueInput("Y")
          .setCheck("Number")
          .appendField("y");
      this.appendValueInput("Z")
          .setCheck("Number")
          .appendField("z");
      this.appendValueInput("RX")
          .setCheck("Number")
          .appendField("rx");
      this.appendValueInput("RY")
          .setCheck("Number")
          .appendField("ry");
      this.appendValueInput("RZ")
          .setCheck("Number")
          .appendField("rz");
      this.setInputsInline(true);
      this.setOutput(true, 'cart-pose');
      this.setColour(193);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['joint_pose'] = {
    init: function() {
      this.appendValueInput("J1")
          .setCheck("Number")
          .appendField("joint-pose")
          .appendField("j1");
      this.appendValueInput("J2")
          .setCheck("Number")
          .appendField("j2");
      this.appendValueInput("J3")
          .setCheck("Number")
          .appendField("j3");
      this.appendValueInput("J4")
          .setCheck("Number")
          .appendField("j4");
      this.appendValueInput("J5")
          .setCheck("Number")
          .appendField("j5");
      this.appendValueInput("J6")
          .setCheck("Number")
          .appendField("j6");
      this.setInputsInline(true);
      this.setOutput(true, 'joint-pose');
      this.setColour(193);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['get_actual_tcp_pose'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("actual tcp pose");
      this.setOutput(true, "Array");
      this.setColour(193);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };
/*
Blockly.Blocks['move'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("move")
          .appendField(new Blockly.FieldDropdown([["joint-space","JOINT_SPACE"], ["tool-space","TOOL_SPACE"], ["circular","CIRCULAR"]]), "MOVE_TYPE");
      this.appendValueInput("Q")
          .setCheck(["position", "rotation"])
          .appendField("pose");
      this.appendValueInput("A")
          .setCheck("Number")
          .appendField("a");
      this.appendValueInput("V")
          .setCheck("Number")
          .appendField("v");
      this.appendValueInput("T")
          .setCheck("Number")
          .appendField("t");
      this.appendValueInput("R")
          .setCheck("Number")
          .appendField("r");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setMutator();
   this.setTooltip("");
   this.setHelpUrl("https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#wyoxkh");
    }
};
*/
Blockly.defineBlocksWithJsonArray([ // BEGIN JSON EXTRACT
    {
        "type": "move",
        "message0": "move %1 %2 pose to %3 pose via %4 a %5 v %6 t %7 r %8",
        "args0": [
          {
            "type": "field_dropdown",
            "name": "MOVE_TYPE",
            "options": [
              ["joint-space","JOINT_SPACE"],
              ["tool-space","TOOL_SPACE"],
              ["circular","CIRCULAR"]
            ]
          },
          {
            "type": "input_dummy"
          },
          {
            "type": "input_value",
            "name": "POSE_TO",
            "check": ["position","rotation"]
          },
          {
            "type": "input_value",
            "name": "POSE_VIA",
            "check": ["position","rotation"]
          },
          {
            "type": "input_value",
            "name": "A",
            "check": "Number"
          },
          {
            "type": "input_value",
            "name": "V",
            "check": "Number"
          },
          {
            "type": "input_value",
            "name": "T",
            "check": "Number"
          },
          {
            "type": "input_value",
            "name": "R",
            "check": "Number"
          }
        ],
        "inputsInline": true,
        "previousStatement": null,
        "nextStatement": null,
        "colour": 150,
        "tooltip": "",
        "helpUrl": "https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#tnt7j2",
        "mutator": "move_mutator"
      }
  ]); // END JSON EXTRACT (Do not delete this comment.)
  
Blockly.Constants.ur_blocks.MOVE_MUTATOR = {
    /**
    * Create XML to represent whether the 'divisorInput' should be present.
    * @return {Element} XML storage element.
    * @this Blockly.Block
    */
    mutationToDom: function() {
        var container = document.createElement('mutation');
        var moveTypeInput = (this.getFieldValue('MOVE_TYPE') == 'CIRCULAR');
        container.setAttribute('moveType_input', moveTypeInput);
        return container;
    },
    /**
    * Parse XML to restore the 'divisorInput'.
    * @param {!Element} xmlElement XML storage element.
    * @this Blockly.Block
    */
    domToMutation: function(xmlElement) {
        var moveTypeInput = (xmlElement.getAttribute('moveType_input') == 'true');
        this.updateShape_(moveTypeInput);  // Helper function for adding/removing 2nd input.
    },
    /**
     * Modify this block to have (or not have) an input for 'is divisible by'.
     * @param {boolean} poseViaInput True if this block has a divisor input.
     * @private
     * @this Blockly.Block
     */
    updateShape_: function(moveTypeInput) {
        var inputExists = this.getInput('POSE_VIA');
        var modeExists = this.getField('MODE');
        var modeParent = this.getSurroundParent();
        if (moveTypeInput) {
            if (!inputExists) {
                this.appendValueInput('POSE_VIA')
                    .setCheck(["position", "rotation"])
                    .appendField("pose via");
            }
        } else if (inputExists) {
            this.removeInput('POSE_VIA');
        }
    }
};
/**
 * 'math_is_divisibleby_mutator' extension to the 'math_property' block that
 * can update the block shape (add/remove divisor input) based on whether
 * property is "divisble by".
 * @this Blockly.Block
 * @package
 */
Blockly.Constants.ur_blocks.MOVE_MUTATOR_EXTENSION = function() {
    this.getField('MOVE_TYPE').setValidator(function(option) {
        var moveTypeInput = (option == 'CIRCULAR');
        this.sourceBlock_.updateShape_(moveTypeInput);
    });
};

Blockly.Extensions.registerMutator('move_mutator',
    Blockly.Constants.ur_blocks.MOVE_MUTATOR,
    Blockly.Constants.ur_blocks.MOVE_MUTATOR_EXTENSION);
/*
// Update the tooltip of 'math_change' block to reference the variable.
Blockly.Extensions.register('math_change_tooltip',
    Blockly.Extensions.buildTooltipWithFieldText(
        '%{BKY_MATH_CHANGE_TOOLTIP}', 'VAR'));
*/
