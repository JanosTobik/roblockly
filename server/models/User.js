const Sequelize = require('sequelize');
const sequelize = require('../config/databaseConfig');
const bcrypt = require('bcryptjs');


const User = sequelize.define('User', {
    userId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: 'userId'
    },
    email: {
        type: Sequelize.STRING,
        unique: 'email',
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    created: {
        type: Sequelize.DATE,
        allowNull: false
    },
    modified: {
        type: Sequelize.DATE
    },
    emailConfirmed: {
        type: Sequelize.BOOLEAN
    },
    confirmationKey: {
        type: Sequelize.STRING
    },
    comment: {
        type: Sequelize.TEXT
    }
}, {
    tableName: 'User',
    hooks: {
        beforeCreate: (user) => {
            const salt = bcrypt.genSaltSync();
            user.password = bcrypt.hashSync(user.password, salt);
        }
    }
});

User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = User;