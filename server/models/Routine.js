const Sequelize = require('sequelize');
const sequelize = require('../config/databaseConfig');

const Routine = sequelize.define('Routine', {
    routineId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: 'routineId'
    },
    routineName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT
    },
    definition: {
        type: Sequelize.TEXT
    },
    created: {
        type: Sequelize.DATE,
        allowNull: false
    },
    modified: {
        type: Sequelize.DATE
    },
    comment: {
        type: Sequelize.TEXT
    }
}, {
    tableName: 'Routine',
    hooks: {
      beforeCreate: (routine) => {
        routine.definition = `<xml xmlns="http://www.w3.org/1999/xhtml"></xml>`
      }
  }
});

module.exports = Routine;