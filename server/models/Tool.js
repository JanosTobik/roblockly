const Sequelize = require('sequelize');
const sequelize = require('../config/databaseConfig');

const Tool = sequelize.define('Tool', {
    toolId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: 'toolId'
    },
    toolName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING
    },
    tcpPose: {
        type: Sequelize.STRING
    },
    payloadMass: {
        type: Sequelize.INTEGER
    },
    payloadCog: {
        type: Sequelize.STRING
    },
    created: {
        type: Sequelize.DATE,
        allowNull: false
    },
    modified: {
        type: Sequelize.DATE
    }
}, {
    tableName: 'Tool'
})

module.exports = Tool;