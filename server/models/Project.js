const Sequelize = require('sequelize');
const sequelize = require('../config/databaseConfig');

const Project = sequelize.define('Project', {
    projectId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: 'projectId'
    },
    projectName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT
    },
    definition: {
        type: Sequelize.TEXT
    },
    created: {
        type: Sequelize.DATE,
        allowNull: false
    },
    modified: {
        type: Sequelize.DATE
    },
    comment: {
        type: Sequelize.TEXT
    }
}, {
    tableName: 'Project',
    hooks: {
        beforeCreate: (project) => {
            project.definition = `<xml xmlns="http://www.w3.org/1999/xhtml"><block id="main_block" type="main_block" deletable="false" x="230" y="130"></block></xml>`
        }
    }
});

module.exports = Project;