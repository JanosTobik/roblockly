const Sequelize = require('sequelize');
const sequelize = require('../config/databaseConfig');

const Pose = sequelize.define('Pose', {
    poseId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: 'poseId'
    },
    poseName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    code: {
        type: Sequelize.STRING
    },
    poseType: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    created: {
        type: Sequelize.DATE,
        allowNull: false
    },
    modified: {
        type: Sequelize.DATE
    }
}, {
    tableName: 'Pose'
})

module.exports = Pose;