const Sequelize = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: process.env.DB_PATH,
    logging: process.env.SQL_LOGGING === 'true' ? console.log : false,
    define : 
    { 
        freezeTableName: true,
        timestamps: false
    }
});

sequelize.models.User;
sequelize.models.Project;
sequelize.models.Routine;
sequelize.models.Pose;
sequelize.models.Tool;

sequelize.authenticate()
.then(() => {
    console.log('Connection established! Current database: ' + sequelize.options.storage.split('/').slice(-1)[0]);
})
.then(async () => {
    
    const User = require('../models/User');
    const Project = require('../models/Project');
    const Routine = require('../models/Routine');
    const Pose = require('../models/Pose');
    const Tool = require('../models/Tool');

    await User.hasMany(Project, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});
    await Project.belongsTo(User, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});

    await User.hasMany(Routine, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});
    await Routine.belongsTo(User, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});

    await User.hasMany(Pose, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});
    await Pose.belongsTo(User, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});

    await User.hasMany(Tool, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});
    await Tool.belongsTo(User, {foreignKey: 'fk_User_userId', onDelete: 'SET NULL'});

    sequelize.sync();
})
.catch((err) => {
    console.log(err)
    console.log('Unable to connect!');
});


module.exports = sequelize;