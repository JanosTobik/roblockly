/**
 * Import node modules
 */
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const schedule = require('node-schedule');
const http = require('http');
if (process.env.NODE_ENV !== 'production') require('dotenv').config();

/**
 * Api
 */
const app = express();
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
   res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
   next();
});
if (process.env.HTTP_LOGGING === 'true') app.use(morgan('dev'));

/**
 * Use static files
 */
app.use(express.static('.'));
app.use(express.static('../client/build'));

/**
 * Import api modules
 */
app.use('/api', require('./routes/robotRoutes'));
app.use('/api', require('./routes/userRoutes'));
app.use('/api', require('./routes/projectRoutes'));
app.use('/api', require('./routes/routineRoutes'));
app.use('/api', require('./routes/authRoutes'));
app.use('/api', require('./routes/rrsmRoutes'));
app.use('/api', require('./routes/capsuleRoutes'));
app.use('/api', require('./routes/poseRoutes'));
app.use('/api', require('./routes/toolRoutes'));
app.use(require('./routes/viewRoutes'));

const userService = require('./services/userService');
const WebsocketConnector = require('./utils/WebsocketConnector');

app.get('*', function (req, res) {
   res.redirect('/404');
});

const server = http.createServer(app);
const websocketConnector = new WebsocketConnector(server);


/**
 * Application entry point
 */
server.listen(process.env.PORT || 8080, function () {
   console.log('Example app listening at '+process.env.PORT);
});

process.on('uncaughtException', function (err) {
   console.log(err);
});


schedule.scheduleJob('0 0 0 * * *', () => {
   userService.deleteExpiredUsers()
});

module.exports = server;