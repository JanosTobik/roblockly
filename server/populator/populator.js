const sequelize = require('../config/databaseConfig');
const User = require('../models/User');
const Project = require('../models/Project');

module.exports = {
    insertDummyData: async function() {
        if (0 === await User.count()) {
            for(var i=1; i<=5; i++) {
                await User.create({
                    userName: 'username'+i,
                    password: 'a',
                    email: 'email'+i+'@example.com',
                    creationTime: new Date().toString(),
                    modified: null,
                    comment: 'dummy record'
                }).catch(err => {console.log(err)});
            }
        }

        if (0 === await Project.count()) {
            for(var i=1; i<=5; i++) {
                await Project.create({
                    projectName: 'projectname'+i,
                    projectDescription: 'desc'+i,
                    projectDefiniton: 'projectdef'+i,
                    creationTime: new Date(),
                    fk_User_userId: i,
                    comment: ''
                }).catch(err => {console.log(err)});;
            }
        }
    }
}