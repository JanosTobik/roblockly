const express = require('express');
const router = express.Router();

const routineHandler = require('../middlewares/routineHandler');
const auth = require('../middlewares/authentication');

router.get('/routines/:id', auth.checkToken, routineHandler.findRoutineById);
router.post('/routine', auth.checkToken, routineHandler.createNewRoutine);
router.put('/routine', auth.checkToken, routineHandler.saveRoutine);
router.delete('/routines/:id', auth.checkToken, routineHandler.deleteRoutine);

module.exports = router;