const express = require('express');
const router = express.Router();

const rrsmHandler = require('../middlewares/rrsmHandler');
const auth = require('../middlewares/authentication');

router.post('/rrsm/service/login', auth.checkToken, rrsmHandler.login);
router.post('/rrsm/service/start', auth.checkToken, rrsmHandler.start);
router.post('/rrsm/service/send', auth.checkToken, rrsmHandler.sendProgram);
router.post('/rrsm/service/delete', auth.checkToken, rrsmHandler.deleteContainer);
router.post('/rrsm/watchdog/restart', auth.checkToken, rrsmHandler.watchdogRestart);

module.exports = router;