const express = require('express');
const router = express.Router();

const toolHandler = require('../middlewares/toolHandler');
const auth = require('../middlewares/authentication');

router.get('/tools/:id', auth.checkToken, toolHandler.findToolById);
router.post('/tool', auth.checkToken, toolHandler.createNewTool);
router.put('/tool', auth.checkToken, toolHandler.saveTool);
router.delete('/tools/:id', auth.checkToken, toolHandler.deleteTool);

module.exports = router;