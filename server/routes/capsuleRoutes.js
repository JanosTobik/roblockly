const express = require('express');
const router = express.Router();

const capsuleHandler = require('../middlewares/capsuleHandler');
const auth = require('../middlewares/authentication');

router.post('/capsule/:sid', auth.checkToken, capsuleHandler.sendProgram);
router.get('/capsule/state/:sid', auth.checkToken, capsuleHandler.getCurrentState);

module.exports = router;