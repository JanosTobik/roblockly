const express = require('express');
const router = express.Router();

const auth = require('../middlewares/authentication');

router.post('/signup', auth.signUp);
router.post('/login', auth.login);
router.get('/index', auth.checkToken, auth.index);

module.exports = router;