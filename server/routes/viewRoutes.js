const express = require('express');
const path = require('path');
const router = express.Router();


router.get(['/:sid/projects',
            '/:sid/routines',
            '/:sid/poses',
            '/:sid/tools',
            '/:sid/projects/:projectName',
            '/:sid/routines/:routineName',
            '/:sid/jogging',
            '/:sid/settings',
            '/:sid/robotstate',
            '/login',
            '/register',
            '/confirm/:key/:success',
            '/404',
            '/500'
        ], function (req, res) {
    res.sendFile(path.resolve('../client/build/index.html'));
});


module.exports = router;