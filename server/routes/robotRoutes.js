const express = require('express');
const router = express.Router();

const urservice = require('../services/URService');
const auth = require('../middlewares/authentication');

router.get('/robot/connect', (req, res) => {
    urservice.setupConnection();
    res.json({
        message: 'Connection established'
    });
});

router.delete('/robot/disconnect', (req, res) => {
    urservice.closeConnection();
    res.json({
        message: 'Disconnected'
    });
});

router.post('/robot/program/ur', auth.checkToken, (req, res) => {
    console.log("Received from browser:\n" + req.body.robotProgram);
    var message = req.body.robotProgram
    urservice.sendMsgToUR(message);
    console.log("#=== END");
    res.json({
        message: 'Program has been sent to UR!'
    });
});

module.exports = router;