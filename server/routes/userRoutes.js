const express = require('express');
const router = express.Router();

const userHandler = require('../middlewares/userHandler');
const auth = require('../middlewares/authentication');

router.get('/users/:id/projects', auth.checkToken, userHandler.findUserProjects);
router.get('/users/:id/routines', auth.checkToken, userHandler.findUserRoutines);
router.get('/users/:id/poses', auth.checkToken, userHandler.findUserPoses);
router.get('/users/:id/tools', auth.checkToken, userHandler.findUserTools);
router.put('/users/:id/password', auth.checkToken, userHandler.changeUserPassword);
router.delete('/users/:id', auth.checkToken, userHandler.deleteUser);
router.get('/confirm/:key', userHandler.confirmEmail);

module.exports = router;