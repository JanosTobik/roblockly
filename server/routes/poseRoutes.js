const express = require('express');
const router = express.Router();

const poseHandler = require('../middlewares/poseHandler');
const auth = require('../middlewares/authentication');

router.get('/poses/:id', auth.checkToken, poseHandler.findPoseById);
router.post('/pose', auth.checkToken, poseHandler.createNewPose);
router.put('/pose', auth.checkToken, poseHandler.savePose);
router.delete('/poses/:id', auth.checkToken, poseHandler.deletePose);

module.exports = router;