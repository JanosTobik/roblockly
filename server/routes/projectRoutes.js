const express = require('express');
const router = express.Router();

const projectHandler = require('../middlewares/projectHandler');
const auth = require('../middlewares/authentication');

router.get('/projects/:id', auth.checkToken, projectHandler.findProjectById);
router.post('/project', auth.checkToken, projectHandler.createNewProject);
router.put('/project', auth.checkToken, projectHandler.saveProject);
router.delete('/projects/:id', auth.checkToken, projectHandler.deleteProject);

module.exports = router;