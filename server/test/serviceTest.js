const expect  = require('chai').expect;
const bcrypt = require('bcryptjs');
const app = require('../app');

const userService = require('../services/userService');
const projectService = require('../services/projectService');
const routineService = require('../services/routineService');
const poseService = require('../services/poseService');

// global variable for later tests
let userId = null;
let projectId = null;
let routineId = null;
let poseId = null;

// close app after tests
after(done => {
    app.close(done);
});

describe('Create objects', () => {
    it('User', done => {
        userService.createNewUser('email@email.com', 'a')
        .then(user => {
            expect(user.email).not.to.be.null;
            expect(user.email).to.equal('email@email.com');

            expect(user.password).not.to.be.null;
            expect(bcrypt.compareSync('a', user.password)).to.be.true;

            userId = user.userId;
            done();
        })
        .catch(() => done(new Error('User cannot be created')))
    });

    it('Project', done => {
        projectService.createNewProject(userId, 'sampleProjectName', 'sampleDescription')
        .then(project => {
            expect(project.projectName).not.to.be.null;
            expect(project.projectName).to.equal('sampleProjectName');

            expect(project.description).not.to.be.null;
            expect(project.description).to.equal('sampleDescription');

            projectId = project.projectId;
            done();
        })
        .catch(err => done(new Error(err)))
    });

    it('Routine', done => {
        routineService.createNewRoutine(userId, 'sampleRoutineName', 'sampleDescription')
        .then(routine => {
            expect(routine.routineName).not.to.be.null;
            expect(routine.routineName).to.equal('sampleRoutineName');

            expect(routine.description).not.to.be.null;
            expect(routine.description).to.equal('sampleDescription');

            routineId = routine.routineId;
            done();
        })
        .catch(err => done(new Error(err)))
    });

    it('Pose', done => {
        poseService.createNewPose(userId, 'samplePoseName', 'sampleCode', 'samplePoseType', 'sampleDescription')
        .then(pose => {
            expect(pose.poseName).not.to.be.null;
            expect(pose.poseName).to.equal('samplePoseName');

            expect(pose.code).not.to.be.null;
            expect(pose.code).to.equal('sampleCode');

            expect(pose.poseType).not.to.be.null;
            expect(pose.poseType).to.equal('samplePoseType');

            expect(pose.description).not.to.be.null;
            expect(pose.description).to.equal('sampleDescription');

            poseId = pose.poseId;
            done();
        })
        .catch(() => done(new Error('Pose cannot be created')))
    });
});

describe('Update objects', () => {
    it('User password', done => {
        userService.changePassword(userId, 'a', 'b')
        .then(user => {
            expect(user.password).not.to.be.null;
            expect(bcrypt.compareSync('b', user.password)).to.be.true;

            done();
        })
        .catch(() => done(new Error('Changing password failed')))
    });

    it('Project', done => {
        projectService.saveProject({fk_User_userId: userId, projectId: projectId, projectName: 'sampleName', description: 'sampleDesc', definition: 'sampleDefinition'})
        .then(project => {
            expect(project.projectName).not.to.be.null;
            expect(project.projectName).to.equal('sampleName');

            expect(project.description).not.to.be.null;
            expect(project.description).to.equal('sampleDesc');

            expect(project.definition).not.to.be.null;
            expect(project.definition).to.equal('sampleDefinition');

            done();
        })
        .catch(() => done(new Error('Project cannot be updated')))
    });
    
    it('Routine', done => {
        routineService.saveRoutine({fk_User_userId: userId, routineId: routineId, routineName: 'sampleName', description: 'sampleDesc', definition: 'sampleDefinition'})
        .then(routine => {
            expect(routine.routineName).not.to.be.null;
            expect(routine.routineName).to.equal('sampleName');

            expect(routine.description).not.to.be.null;
            expect(routine.description).to.equal('sampleDesc');

            expect(routine.definition).not.to.be.null;
            expect(routine.definition).to.equal('sampleDefinition');

            done();
        })
        .catch(() => done(new Error('Routine cannot be updated')))
    });
    
    it('Pose', done => {
        poseService.savePose({fk_User_userId: userId, poseId: poseId, poseName: 'sampleName', description: 'sampleDesc', code: 'code', poseType: 'type'})
        .then(pose => {
            expect(pose.poseName).not.to.be.null;
            expect(pose.poseName).to.equal('sampleName');

            expect(pose.description).not.to.be.null;
            expect(pose.description).to.equal('sampleDesc');

            expect(pose.code).not.to.be.null;
            expect(pose.code).to.equal('code');

            expect(pose.poseType).not.to.be.null;
            expect(pose.poseType).to.equal('type');

            done();
        })
        .catch(() => done(new Error('Pose cannot be updated')))
    });
});

describe('Find objects', () => {
    it('User by email', done => {
        userService.findUserByEmail('email@email.com')
        .then(user => {
            expect(user).not.to.be.null;
            expect(user.userId).to.equal(userId);
            done();
        })
        .catch(() => done(new Error('User not found')))
    });

    it('User projects', done => {
        userService.findUserProjects(userId)
        .then(projects => {
            expect(projects).to.have.lengthOf(1);
            done();
        })
        .catch(() => done(new Error('Projects not found')))
    });

    it('User routines', done => {
        userService.findUserRoutines(userId)
        .then(routines => {
            expect(routines).to.have.lengthOf(1);
            done();
        })
        .catch(() => done(new Error('Routines not found')))
    });

    it('User poses', done => {
        userService.findUserPoses(userId)
        .then(poses => {
            expect(poses).to.have.lengthOf(1);
            done();
        })
        .catch(() => done(new Error('Poses not found')))
    });
});

describe('Delete objects', () => {
    it('Pose', done => {
        poseService.deletePose(poseId)
        .then(() => done())
        .catch(() => done(new Error('Pose cannot be deleted')))
    });

    it('Routine', done => {
        routineService.deleteRoutine(routineId)
        .then(() => done())
        .catch(err => done(new Error(err)))
    });

    it('Project', done => {
        projectService.deleteProject(projectId)
        .then(() => done())
        .catch(() => done(new Error('Project cannot be deleted')))
    });

    it('User', done => {
        userService.deleteUser(userId)
        .then(() => done())
        .catch(() => done(new Error('User cannot be deleted')))
    });
});