const EventEmitter = require('events');
const net = require('net');

class URConnector extends EventEmitter {
    constructor (PORT, HOST) {
        super();
        this.client = new net.Socket();
        this.client.connect(PORT, HOST, () => console.log(`Connected to ${HOST}:${PORT}`));
        this.client.setNoDelay(true);
        
        this.on('write', data =>{
            this.client.write(data);
        });
        this.on('disconnect', () => {
            this.client.destroy();
            console.log(`Disconnected from ${HOST}:${PORT}`);
        });

        this.client.on('error', error => {
            console.log('error: ', error)
            this.client.destroy();
        });
    }
}

module.exports = URConnector;