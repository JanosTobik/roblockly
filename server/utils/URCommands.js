//
// relative cartesian commands
//
const NegXX = '  movel(pose_add(get_actual_tool_flange_pose(), p[-0.05, 0, 0, 0, 0, 0]), a=1, v=1)\n';
const PosXX = '  movel(pose_add(get_actual_tool_flange_pose(), p[0.05, 0, 0, 0, 0, 0]), a=1, v=1)\n';
const NegYY = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, -0.05, 0, 0, 0, 0]), a=1, v=1)\n';
const PosYY = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0.05, 0, 0, 0, 0]), a=1, v=1)\n';
const NegZZ = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, -0.05, 0, 0, 0]), a=1, v=1)\n';
const PosZZ = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0.05, 0, 0, 0]), a=1, v=1)\n';
const NegRX = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0, -0.05, 0, 0]), a=1, v=1)\n';
const PosRX = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0, 0.05, 0, 0]), a=1, v=1)\n';
const NegRY = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0, 0, -0.05, 0]), a=1, v=1)\n';
const PosRY = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0, 0, 0.05, 0]), a=1, v=1)\n';
const NegRZ = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0, 0, 0, -0.05]), a=1, v=1)\n';
const PosRZ = '  movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0, 0, 0, 0.05]), a=1, v=1)\n';
//
// relative joint commands
//
const NegJ1 = this.jointPoseTemplate(-0.034906585, 0, 0, 0, 0, 0);
const PosJ1 = this.jointPoseTemplate(0.034906585, 0, 0, 0, 0, 0);
const NegJ2 = this.jointPoseTemplate(0, -0.1, 0, 0, 0, 0);
const PosJ2 = this.jointPoseTemplate(0, 0.1, 0, 0, 0, 0);
const NegJ3 = this.jointPoseTemplate(0, 0, -0.1, 0, 0, 0);
const PosJ3 = this.jointPoseTemplate(0, 0, 0.1, 0, 0, 0);
const NegJ4 = this.jointPoseTemplate(0, 0, 0, -0.1, 0, 0);
const PosJ4 = this.jointPoseTemplate(0, 0, 0, 0.1, 0, 0);
const NegJ5 = this.jointPoseTemplate(0, 0, 0, 0, -0.1, 0);
const PosJ5 = this.jointPoseTemplate(0, 0, 0, 0, 0.1, 0);
const NegJ6 = this.jointPoseTemplate(0, 0, 0, 0, 0, -0.1);
const PosJ6 = this.jointPoseTemplate(0, 0, 0, 0, 0, 0.1);

function functionTemplate(code) {
    const functionStart = 'def MyFunction():\n';
    const functionEnd = '\nend\n';
    return functionStart + code + functionEnd;
}

function jointPoseTemplate(j1=0, j2=0, j3=0, j4=0, j5=0, j6=0) {
    let code =
    `arr = get_actual_joint_positions()
    movej([arr[0] + ${j1}, arr[1] + ${j2}, arr[2] + ${j3}, arr[3] + ${j4}, arr[4] + ${j5}, arr[5] + ${j6}], a=1, v=1)`.replace(/^[ ]*/gm, '  ');
    return code;
}

module.exports = {
    codeNegXX: functionTemplate(NegXX),
    codePosXX: functionTemplate(PosXX),
    codeNegYY: functionTemplate(NegYY),
    codePosYY: functionTemplate(PosYY),
    codeNegZZ: functionTemplate(NegZZ),
    codePosZZ: functionTemplate(PosZZ),
    codeNegRX: functionTemplate(NegRX),
    codePosRX: functionTemplate(PosRX),
    codeNegRY: functionTemplate(NegRY),
    codePosRY: functionTemplate(PosRY),
    codeNegRZ: functionTemplate(NegRZ),
    codePosRZ: functionTemplate(PosRZ),

    codeNegJ1: functionTemplate(NegJ1),
    codePosJ1: functionTemplate(PosJ1),
    codeNegJ2: functionTemplate(NegJ2),
    codePosJ2: functionTemplate(PosJ2),
    codeNegJ3: functionTemplate(NegJ3),
    codePosJ3: functionTemplate(PosJ3),
    codeNegJ4: functionTemplate(NegJ4),
    codePosJ4: functionTemplate(PosJ4),
    codeNegJ5: functionTemplate(NegJ5),
    codePosJ5: functionTemplate(PosJ5),
    codeNegJ6: functionTemplate(NegJ6),
    codePosJ6: functionTemplate(PosJ6),
}