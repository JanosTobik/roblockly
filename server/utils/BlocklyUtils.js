module.exports = {
    defaultProject: `
<xml xmlns="http://www.w3.org/1999/xhtml">
<variables>
    <variable type="">index</variable>
</variables>
<block type="main_block" x="230" y="230">
    <field name="FunctionName">MyFunction</field>
    <statement name="STACK">
        <block type="variables_set" >
            <field name="VAR" variabletype="">index</field>
            <value name="VALUE">
                <block type="math_number" >
                    <field name="NUM">0</field>
                </block>
            </value>
            <next>
                <block type="controls_whileUntil" >
                    <field name="MODE">WHILE</field>
                    <value name="BOOL">
                        <block type="logic_compare" >
                            <field name="OP">LT</field>
                            <value name="A">
                                <block type="variables_get" >
                                    <field name="VAR" variabletype="">index</field>
                                </block>
                            </value>
                            <value name="B">
                                <block type="math_number" >
                                    <field name="NUM">3</field>
                                </block>
                            </value>
                        </block>
                    </value>
                    <statement name="DO">
                        <block type="move_joint" >
                            <field name="ABS_REL">RELATIVE</field>
                            <value name="JPOSE">
                                <block type="joint_pose" >
                                    <value name="J1">
                                        <block type="math_number" >
                                            <field name="NUM">1</field>
                                        </block>
                                    </value>
                                    <value name="J2">
                                        <block type="math_number" >
                                            <field name="NUM">0</field>
                                        </block>
                                    </value>
                                    <value name="J3">
                                        <block type="math_number" >
                                            <field name="NUM">0</field>
                                        </block>
                                    </value>
                                    <value name="J4">
                                        <block type="math_number" >
                                            <field name="NUM">0</field>
                                        </block>
                                    </value>
                                    <value name="J5">
                                        <block type="math_number" >
                                            <field name="NUM">0</field>
                                        </block>
                                    </value>
                                    <value name="J6">
                                        <block type="math_number" >
                                            <field name="NUM">0</field>
                                        </block>
                                    </value>
                                </block>
                            </value>
                            <value name="ANG_ACCELERATION">
                                <block type="math_number" >
                                    <field name="NUM">0.5</field>
                                </block>
                            </value>
                            <value name="ANG_VELOCITY">
                                <block type="math_number" >
                                    <field name="NUM">1</field>
                                </block>
                            </value>
                            <next>
                                <block type="sleep" >
                                    <field name="SLEEP">1</field>
                                    <next>
                                        <block type="move_joint" >
                                            <field name="ABS_REL">RELATIVE</field>
                                            <value name="JPOSE">
                                                <block type="joint_pose" >
                                                    <value name="J1">
                                                        <block type="math_number" >
                                                            <field name="NUM">-1</field>
                                                        </block>
                                                    </value>
                                                    <value name="J2">
                                                        <block type="math_number" >
                                                            <field name="NUM">0</field>
                                                        </block>
                                                    </value>
                                                    <value name="J3">
                                                        <block type="math_number" >
                                                            <field name="NUM">0</field>
                                                        </block>
                                                    </value>
                                                    <value name="J4">
                                                        <block type="math_number" >
                                                            <field name="NUM">0</field>
                                                        </block>
                                                    </value>
                                                    <value name="J5">
                                                        <block type="math_number" >
                                                            <field name="NUM">0</field>
                                                        </block>
                                                    </value>
                                                    <value name="J6">
                                                        <block type="math_number" >
                                                            <field name="NUM">0</field>
                                                        </block>
                                                    </value>
                                                </block>
                                            </value>
                                            <value name="ANG_ACCELERATION">
                                                <block type="math_number" >
                                                    <field name="NUM">0.5</field>
                                                </block>
                                            </value>
                                            <value name="ANG_VELOCITY">
                                                <block type="math_number" >
                                                    <field name="NUM">1</field>
                                                </block>
                                            </value>
                                            <next>
                                                <block type="math_change" >
                                                    <field name="VAR" variabletype="">index</field>
                                                    <value name="DELTA">
                                                        <shadow type="math_number" >
                                                            <field name="NUM">1</field>
                                                        </shadow>
                                                        <block type="math_number" >
                                                            <field name="NUM">1</field>
                                                        </block>
                                                    </value>
                                                </block>
                                            </next>
                                        </block>
                                    </next>
                                </block>
                            </next>
                        </block>
                    </statement>
                </block>
            </next>
        </block>
    </statement>
</block>
</xml>`
}