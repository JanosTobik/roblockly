const net = require('net');
const fs = require('fs');

const ur_address = process.env.UR_IP;
const ur_port = process.env.UR_PORT;


var client = new net.Socket();
var recvdata = "";

module.exports = {
	connect : function() {
		try {
			if(!client.destroyed) {
				return;
			}
			client.connect(ur_port, ur_address);
			console.log('#=== Connection established');
			client.setNoDelay(true);
		} catch(ex) {
			console.log('#=== conn exception')
		}
	},

	closeConnection : function() {
		console.log('#=== Connection closed');
		client.end();
	},

	sendMessage : function(msg) {
		try {
			console.log('#=== Sending data');
			
			client.write(msg);
			console.log(msg);
			console.log('#=== Data sent');
		} catch(ex) {
			console.log('#=== write exception');
			console.log(ex);
		}
	}
}


client.on('error', function(err) {
	console.log(err);
	console.log(err.stack);
});

client.on('data', function(data) {
	//recvdata = Buffer.from(data, 'utf8').toString('hex') + '\n';
	//Buffer.from(data, 'utf8').toString('hex')
	//JSON.stringify(data,null,4)
	//require('util').inspect(data, true, 20, true)
	
	//console.log('#=== Received:' + data.toString());
	//console.log('#=== Data received');
	
	/*fs.appendFile('./OUTPUT.txt', JSON.stringify(data,null,4), function(err) {
		if(err) {
			return console.log(err);
		}
		console.log('#=== Output file saved');
	});*/
});

client.on('close', function() {
	console.log('#=== Connection closed');
});
