const EventEmitter = require('events');
const ieee754 = require('ieee754')
const net = require('net');
const urStateDef = require('./URStateDef')

var currentState = {}

class URStateReceiver extends EventEmitter {
    constructor(PORT, HOST) {
        super();
        this.client = new net.Socket();
        this.client.connect(PORT, HOST, () => console.log(`Connected to ${HOST}:${PORT}`));

        this.client.on('data', data => {
            for (var key in urStateDef) {
                currentState[key] = ieee754.read(
                    data,
                    urStateDef[key].start,
                    false,
                    52,
                    urStateDef[key].length
                )
            }
        });

        this.client.on('data', () => {
            this.emit('data', currentState)
        });
        this.client.on('error', error => {
            console.log('error: ', error)
            this.client.destroy();
        });

        this.on('disconnect', () => {
            this.client.destroy();
            console.log(`Disconnected from ${HOST}:${PORT}`);
        });
    }

    getCurrentState() {
        return currentState;
    }
}

module.exports = URStateReceiver;