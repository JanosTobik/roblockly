module.exports = {
    confirmTemplate: function (key) {
        return {
            subject: 'Roblockly Email Confirmation',
            html: `
            <a href='${process.env.URL}/api/confirm/${key}'>
                click to confirm email
            </a>`,
            text: `Copy and paste this link: ${process.env.URL}/api/confirm/${key}`
        }
    },

    expiredTemplate: function() {
        return {
            subject: 'Roblockly Expired User',
            html: `
            <h4>Dear User,</br></h4>
            <p> Your confirmation link has been expired and your account has been removed!</p>`,
            text: `Your confirmation link has been expired and your account has been removed!`
        }
    }
}