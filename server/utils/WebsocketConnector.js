
const socketIo = require('socket.io');
const net = require('net');

const URConnector = require('./URConnector');
const URStateReceiver = require('./URStateReceiver');

class WebsocketConnector {
    constructor(server) {
        const io = socketIo(server);
        io.on("connection", socket => {
            console.log("New client connected " + socket.id);
            let client = new net.Socket();
            let urStateReceiver = null;
            let interval = null;
            socket.on('connectToCapsule', data => {
                client = new URConnector(30002, data.sid);
                urStateReceiver = new URStateReceiver(30003, data.sid);
                
                interval = setInterval(() => {
                    let currentState = urStateReceiver.getCurrentState();
                    let { actXXpos, actYYpos, actZZpos, actRXpos, actRYpos, actRZpos, actJ1pos, actJ2pos, actJ3pos, actJ4pos, actJ5pos, actJ6pos } = currentState;
                    socket.emit('robotstate', { actXXpos, actYYpos, actZZpos, actRXpos, actRYpos, actRZpos, actJ1pos, actJ2pos, actJ3pos, actJ4pos, actJ5pos, actJ6pos });
                }, 700)
            });
            socket.on('jogging', data => {
                client.emit('write', data.program)
            });
            socket.on('disconnect', () => {
                client.emit('disconnect');
                if (urStateReceiver !== null) {
                    urStateReceiver.emit('disconnect');
                }
                clearInterval(interval);
            });
        });
    }
}

module.exports = WebsocketConnector;