const net = require('net');
const URStateReceiver = require('../utils/URStateReceiver');

module.exports = {
    sendProgram: (req, res, next) => {
        let sid = req.params.sid;
        let program = req.body.robotProgram;

        let client = new net.Socket();
        try {
            client.connect(30002, sid);
            client.setNoDelay(true);
            client.on('error', () => {
                client.end();
                res.status(500).json({
                    success: false,
                    message: `${sid} tcp error!`
                })
            });
            client.write(program);
            client.end();
            res.status(200).json({
                success: true,
                message: `Program has been sent to ${sid}`
            })
        } catch(ex) {
            console.log(ex);
            res.status(500).json({
                success: false,
                message: `Sending program to ${sid} failed`
            })
        }
    },

    getCurrentState: (req, res, next) => {
        let sid = req.params.sid;

        const urStateDataInst = new URStateReceiver(30003, sid);

        let promise = new Promise((resolve, reject) => {
            urStateDataInst.on('data', (data) => resolve(data));
        });

        promise.then(state => {
            urStateDataInst.client.destroy();
            res.status(200).json({
                success: true,
                message: `${sid} current state`,
                data: state
            })
        })
        .catch(() => res.status(500).json({
            success: false,
            message: `${sid} current state error`
        }));
    }
}