const axios = require('axios');

const RRSM_url = process.env.RRSM_URL;

module.exports = {
    login: (req, res, next) => {
        axios.post(`${RRSM_url}/service/login`, {
            name: 'service',
            password: 'password'
        })
        .then(resp => {
            res.status(200).json({
                success: true,
                message: 'RRSM token gained!',
                token: resp.data.token
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'RRSM token not given!'
            });
        });
    },

    start: (req, res, next) => {
        let token = req.body.token;
		
        axios.post(`${RRSM_url}/service/start`, null,
        {
            headers: {
                'authorization': `Bearer ${token}`
            }
        })
        .then(resp => {
            res.status(200).json({
                success: true,
                message: 'SID gained!',
                data: {
                    sid: resp.data.sid,
                    rosPort: resp.data.rosPort,
                    urPort: resp.data.urPort
                }
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'RRSM start failed!'
            });
        });
        
    },

    sendProgram: (req, res, next) => {
        let urScript = req.body.robotProgram;
        let token = req.body.token;
        let sid = req.body.sid;

        axios.post(`${RRSM_url}/service/${sid}`, {
            program: urScript
        }, {
            headers: {
                'authorization': `Bearer ${token}`
            }
        })
        .then(resp => {
            res.status(200).json({
                success: true,
                message: 'Sending program succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Sending program failed!'
            });
        });
    },
	
	deleteContainer: (req, res, next) => {
		let token = req.body.token;
        let sid = req.body.sid;

		axios.delete(`${RRSM_url}/service/${sid}`,
		{
            headers: {
                'authorization': `Bearer ${token}`
            }
        })
		.then(resp => {
            res.status(200).json({
                success: true,
                message: 'Delete container succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Delete container failed!'
            });
        });
    },
    
    watchdogRestart: (req, res, next) => {
        let sid = req.body.sid;
        let token = req.body.token;

        axios.post(`${RRSM_url}/watchdog/${sid}`, null,
		{
            headers: {
                'authorization': `Bearer ${token}`
            }
        })
        .then(resp => {
            res.status(200).json({
                success: true,
                message: 'Watchdog restarted!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Watchdog restart failed!'
            });
        });
    }
}