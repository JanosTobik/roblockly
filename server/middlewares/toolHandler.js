const toolService = require('../services/toolService');
const userService = require('../services/userService');

module.exports = {
    findToolById: (req, res, next) => {
        toolService.findToolById(req.params.id)
        .then(tool => {
            res.status(200).json({
                success: true,
                message: 'Tool found!',
                data: tool
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'Tool not found!'
            });
        });
    },

    createNewTool: (req, res, next) => {
        userService.findUserById(req.body.userId)
        .then(() => {
            toolService.createNewTool(
                req.body.userId, req.body.toolName, req.body.description,
                req.body.tcpPose, req.body.payloadMass, req.body.payloadCog)
            .then(() => {
                res.status(201).json({
                    success: true,
                    message: 'Creating new tool succeeded!'
                });
            })
            .catch(() => {
                res.status(500).json({
                    success: false,
                    message: 'Creating new tool failed!'
                });
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User not found'
            });
        });
    },

    deleteTool: (req, res, next) => {
        toolService.deleteTool(req.params.id)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Delete tool succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Delete tool failed!'
            });
        });
    },

    saveTool: (req, res, next) => {
        toolService.saveTool(req.body)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Update tool succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Update tool failed!'
            });
        });
    },
}