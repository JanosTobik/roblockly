const routineService = require('../services/routineService');
const userService = require('../services/userService');

module.exports = {
    findRoutineById: (req, res, next) => {
        routineService.findRoutineById(req.params.id)
        .then(routine => {
            res.status(200).json({
                success: true,
                message: 'Routine found!',
                data: routine
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'Routine not found!'
            });
        });
    },
    
    createNewRoutine: (req, res, next) => {
        userService.findUserById(req.body.userId)
        .then(() => {
            routineService.createNewRoutine(req.body.userId, req.body.routineName, req.body.description)
            .then(() => {
                res.status(201).json({
                    success: true,
                    message: 'Creating new routine succeeded!'
                });
            })
            .catch(() => {
                res.status(500).json({
                    success: false,
                    message: 'Creating new routine failed!'
                });
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User not found'
            });
        });
    },

    saveRoutine: (req, res, next) => {
        routineService.saveRoutine(req.body)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Update routine succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Update routine failed!'
            });
        });
    },

    deleteRoutine: (req, res, next) => {
        routineService.deleteRoutine(req.params.id)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Delete routine succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Delete routine failed!'
            });
        });
    }   
}