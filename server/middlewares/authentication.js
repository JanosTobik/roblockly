const jwt = require('jsonwebtoken');

const userService = require('../services/userService');
const emailService = require('../services/emailService');
const emailTemplates = require('../utils/emailTemplates');
const blocklyUtils = require('../utils/BlocklyUtils');
const projectService = require('../services/projectService');

module.exports = {
    signUp: (req, res, next) => {
        userService.findUserByEmail(req.body.email)
        .then(() => {
            res.status(400).json({
                success: false,
                message: 'User already exists!'
            });
        })
        .catch(() => {
            userService.createNewUser(
                req.body.email,
                req.body.password
            )
            .then(user => {
                projectService.createNewProject(user.userId, 'My first project', 'Description of my project')
                .then(project => {
                    project.definition = blocklyUtils.defaultProject;
                    project.save().catch(err => console.log(err));
                })
                .catch(err => console.log(err));
                emailService.sendMail(user.email, emailTemplates.confirmTemplate(user.confirmationKey));
                res.status(201).json({
                    success: true,
                    message: 'Registration succeeded!'
                });
            })
            .catch(() => {
                res.status(500).json({
                    success: false,
                    message: 'Registration error!'
                });
            });
        });
    },

    login: (req, res, next) => {
        var email = req.body.email;
        var password = req.body.password;

        const errorMsg = {
            success: false,
            message: 'Incorrect credentials!'
        };

        userService.findUserByEmail(email)
        .then(user => {
            if (!user.validPassword(password)) {
                console.log('Invalid password!');
                res.status(400).json(errorMsg);
            } else if (!user.emailConfirmed) {
                res.status(400).json({
                    success: false,
                    message: 'Your email is not confirmed yet!'
                });
            } else {
                var token = jwt.sign(
                    {
                        userId: user.userId,
                        email: email
                    },
                    process.env.TOKEN_SECRET,
                    {
                        expiresIn: 120 * 60  //120 min
                    }
                );
                
                res.status(200).json({
                    success: true,
                    message: 'Authentication succeeded!',
                    token: token
                });
            }
        })
        .catch(() => {
            console.log('No user present');
            res.status(404).json(errorMsg);
        })
    },

    checkToken: (req, res, next) => {
        var token = req.headers['x-access-token'] || req.headers['authorization'];

        if (token) {
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }
            
            jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        message: 'Token is not valid!'
                    });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            return res.status(403).json({
                success: false,
                message: 'Auth token does not exist!'
            });
        }
    },

    index: (req, res, next) => {
        res.json({
            success: true,
            message: 'Index page'
        });
    }
};