const poseService = require('../services/poseService');
const userService = require('../services/userService');

module.exports = {
    findPoseById: (req, res, next) => {
        poseService.findPoseById(req.params.id)
        .then(pose => {
            res.status(200).json({
                success: true,
                message: 'Pose found!',
                data: pose
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'Pose not found!'
            });
        });
    },

    createNewPose: (req, res, next) => {
        userService.findUserById(req.body.userId)
        .then(() => {
            poseService.createNewPose(req.body.userId, req.body.poseName, req.body.code, req.body.poseType, req.body.description)
            .then(() => {
                res.status(201).json({
                    success: true,
                    message: 'Creating new pose succeeded!'
                });
            })
            .catch(() => {
                res.status(500).json({
                    success: false,
                    message: 'Creating new pose failed!'
                });
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User not found'
            });
        });
    },

    deletePose: (req, res, next) => {
        poseService.deletePose(req.params.id)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Delete pose succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Delete pose failed!'
            });
        });
    },

    savePose: (req, res, next) => {
        poseService.savePose(req.body)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Update pose succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Update pose failed!'
            });
        });
    },
}