const userService = require('../services/userService');

module.exports = {
    
    findUserProjects: (req, res, next) => {
        userService.findUserProjects(req.params.id)
        .then(projects => {
            res.status(200).json({
                success: true,
                message: 'Projects found!',
                data: projects
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User projects not found!'
            });
        });
    },

    findUserRoutines: (req, res, next) => {
        userService.findUserRoutines(req.params.id)
        .then(routines => {
            res.status(200).json({
                success: true,
                message: 'Routines found!',
                data: routines
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User routines not found!'
            });
        });
    },

    findUserPoses: (req, res, next) => {
        userService.findUserPoses(req.params.id)
        .then(poses => {
            res.status(200).json({
                success: true,
                message: 'Poses found!',
                data: poses
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User poses not found!'
            });
        });
    },

    findUserTools: (req, res, next) => {
        userService.findUserTools(req.params.id)
        .then(tools => {
            res.status(200).json({
                success: true,
                message: 'Tools found!',
                data: tools
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User tools not found!'
            });
        });
    },

    changeUserPassword: (req, res, next) => {
        userService.changePassword(req.params.id, req.body.oldPassword, req.body.newPassword)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Password has been changed!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Password change failed!'
            });
        });
    },

    deleteUser: (req, res, next) => {
        userService.deleteUser(req.params.id)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Delete user succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Delete user failed!'
            });
        });
    },

    confirmEmail: (req, res, next) => {
        userService.confirmEmail(req.params.key)
        .then(() => {
            res.redirect(301, `/confirm/${req.params.key}/verified`);
        })
        .catch(() => {
            res.redirect(301, `/confirm/${req.params.key}/error`);
        });
    }
}