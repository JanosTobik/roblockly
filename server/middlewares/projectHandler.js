const projectService = require('../services/projectService');
const userService = require('../services/userService');

module.exports = {
    findProjectById: (req, res, next) => {
        projectService.findProjectById(req.params.id)
        .then(project => {
            res.status(200).json({
                success: true,
                message: 'Project found!',
                data: project
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'Project not found!'
            });
        });
    },
    
    createNewProject: (req, res, next) => {
        userService.findUserById(req.body.userId)
        .then(() => {
            projectService.createNewProject(req.body.userId, req.body.projectName, req.body.description)
            .then(() => {
                res.status(201).json({
                    success: true,
                    message: 'Creating new project succeeded!'
                });
            })
            .catch(() => {
                res.status(500).json({
                    success: false,
                    message: 'Creating new project failed!'
                });
            });
        })
        .catch(() => {
            res.status(404).json({
                success: false,
                message: 'User not found'
            });
        });
    },

    saveProject: (req, res, next) => {
        projectService.saveProject(req.body)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Update project succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Update project failed!'
            });
        });
    },

    deleteProject: (req, res, next) => {
        projectService.deleteProject(req.params.id)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Delete project succeeded!'
            });
        })
        .catch(() => {
            res.status(500).json({
                success: false,
                message: 'Delete project failed!'
            });
        });
    }
}