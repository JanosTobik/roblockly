/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating Python for blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.UR_script');

goog.require('Blockly.Generator');


Blockly.UR_script = new Blockly.Generator('UR_script');

/**
 * List of illegal variable names.
 * This is not intended to be a security feature.  Blockly is 100% client-side,
 * so bypassing this list is trivial.  This is intended to prevent users from
 * accidentally clobbering a built-in object or function.
 * @private
 */
Blockly.UR_script.addReservedWords(
    'movej,movel,movep,sleep,def,if'
);

/**
 * Order of operation ENUMs.
 */
Blockly.UR_script.ORDER_ATOMIC = 0;            // 0 "" ...
Blockly.UR_script.ORDER_COLLECTION = 1;        // tuples, lists, dictionaries
Blockly.UR_script.ORDER_STRING_CONVERSION = 1; // `expression...`
Blockly.UR_script.ORDER_MEMBER = 2.1;          // . []
Blockly.UR_script.ORDER_FUNCTION_CALL = 2.2;   // ()
Blockly.UR_script.ORDER_EXPONENTIATION = 3;    // **
Blockly.UR_script.ORDER_UNARY_SIGN = 4;        // + -
Blockly.UR_script.ORDER_BITWISE_NOT = 4;       // ~
Blockly.UR_script.ORDER_MULTIPLICATIVE = 5;    // * / // %
Blockly.UR_script.ORDER_ADDITIVE = 6;          // + -
Blockly.UR_script.ORDER_BITWISE_SHIFT = 7;     // << >>
Blockly.UR_script.ORDER_BITWISE_AND = 8;       // &
Blockly.UR_script.ORDER_BITWISE_XOR = 9;       // ^
Blockly.UR_script.ORDER_BITWISE_OR = 10;       // |
Blockly.UR_script.ORDER_RELATIONAL = 11;       // in, not in, is, is not,
                                            //     <, <=, >, >=, <>, !=, ==
Blockly.UR_script.ORDER_LOGICAL_NOT = 12;      // not
Blockly.UR_script.ORDER_LOGICAL_AND = 13;      // and
Blockly.UR_script.ORDER_LOGICAL_OR = 14;       // or
Blockly.UR_script.ORDER_CONDITIONAL = 15;      // if else
Blockly.UR_script.ORDER_LAMBDA = 16;           // lambda
Blockly.UR_script.ORDER_NONE = 99;             // (...)

/**
 * List of outer-inner pairings that do NOT require parentheses.
 * @type {!Array.<!Array.<number>>}
 */
Blockly.UR_script.ORDER_OVERRIDES = [
    // (foo()).bar -> foo().bar
    // (foo())[0] -> foo()[0]
    [Blockly.UR_script.ORDER_FUNCTION_CALL, Blockly.UR_script.ORDER_MEMBER],
    // (foo())() -> foo()()
    [Blockly.UR_script.ORDER_FUNCTION_CALL, Blockly.UR_script.ORDER_FUNCTION_CALL],
    // (foo.bar).baz -> foo.bar.baz
    // (foo.bar)[0] -> foo.bar[0]
    // (foo[0]).bar -> foo[0].bar
    // (foo[0])[1] -> foo[0][1]
    [Blockly.UR_script.ORDER_MEMBER, Blockly.UR_script.ORDER_MEMBER],
    // (foo.bar)() -> foo.bar()
    // (foo[0])() -> foo[0]()
    [Blockly.UR_script.ORDER_MEMBER, Blockly.UR_script.ORDER_FUNCTION_CALL],
  
    // not (not foo) -> not not foo
    [Blockly.UR_script.ORDER_LOGICAL_NOT, Blockly.UR_script.ORDER_LOGICAL_NOT],
    // a and (b and c) -> a and b and c
    [Blockly.UR_script.ORDER_LOGICAL_AND, Blockly.UR_script.ORDER_LOGICAL_AND],
    // a or (b or c) -> a or b or c
    [Blockly.UR_script.ORDER_LOGICAL_OR, Blockly.UR_script.ORDER_LOGICAL_OR]
  ];

  
/**
 * Initialise the database of variable names.
 * @param {!Blockly.Workspace} workspace Workspace to generate code from.
 */
Blockly.UR_script.init = function(workspace) {
    
    // Create a dictionary of definitions to be printed before the code.
    Blockly.UR_script.definitions_ = Object.create(null);
    // Create a dictionary mapping desired function names in definitions_
    // to actual function names (to avoid collisions with user functions).
    Blockly.UR_script.functionNames_ = Object.create(null);
  
    if (!Blockly.UR_script.variableDB_) {
      Blockly.UR_script.variableDB_ =
          new Blockly.Names(Blockly.UR_script.RESERVED_WORDS_);
    } else {
      Blockly.UR_script.variableDB_.reset();
    }
  
    Blockly.UR_script.variableDB_.setVariableMap(workspace.getVariableMap());
  
    var defvars = [];
    // Add developer variables (not created or named by the user).
    var devVarList = Blockly.Variables.allDeveloperVariables(workspace);
    for (var i = 0; i < devVarList.length; i++) {
      defvars.push(Blockly.UR_script.variableDB_.getName(devVarList[i],
          Blockly.Names.DEVELOPER_VARIABLE_TYPE) + ' = None');
    }
  
    // Add user variables, but only ones that are being used.
    var variables = Blockly.Variables.allUsedVarModels(workspace);
    for (var i = 0; i < variables.length; i++) {
      defvars.push(Blockly.UR_script.variableDB_.getName(variables[i].getId(),
          Blockly.Variables.NAME_TYPE) + ' = None');
    }
  
    Blockly.UR_script.definitions_['variables'] = defvars.join('\n');
  };

  
/**
 * Prepend the generated code with the variable definitions.
 * @param {string} code Generated code.
 * @return {string} Completed code.
 */
Blockly.UR_script.finish = function(code) {
    // Convert the definitions dictionary into a list.
    var imports = [];
    var definitions = [];
    
    // Clean up temporary data.
    delete Blockly.UR_script.definitions_;
    delete Blockly.UR_script.functionNames_;
    Blockly.UR_script.variableDB_.reset();
    var allDefs = imports.join('\n') + '\n\n' + definitions.join('\n\n');
    return allDefs.replace(/\n\n+/g, '\n\n').replace(/\n*$/, '\n\n\n') + code;
  };
  
  /**
   * Naked values are top-level blocks with outputs that aren't plugged into
   * anything.
   * @param {string} line Line of generated code.
   * @return {string} Legal line of code.
   */
  Blockly.UR_script.scrubNakedValue = function(line) {
    return line + '\n';
  };
  
  /**
   * Encode a string as a properly escaped Python string, complete with quotes.
   * @param {string} string Text to encode.
   * @return {string} Python string.
   * @private
   */
  Blockly.UR_script.quote_ = function(string) {
    // Can't use goog.string.quote since % must also be escaped.
    string = string.replace(/\\/g, '\\\\')
                   .replace(/\n/g, '\\\n');
  
    // Follow the CPython behaviour of repr() for a non-byte string.
    var quote = '"';
    if (string.indexOf('\'') !== -1) {
      if (string.indexOf('"') === -1) {
        quote = '"';
      } else {
        string = string.replace(/'/g, '\\\'');
      }
    };
    return quote + string + quote;
  };
  
  /**
   * Common tasks for generating Python from blocks.
   * Handles comments for the specified block and any connected value blocks.
   * Calls any statements following this block.
   * @param {!Blockly.Block} block The current block.
   * @param {string} code The Python code created for this block.
   * @return {string} Python code with comments and subsequent blocks added.
   * @private
   */
  Blockly.UR_script.scrub_ = function(block, code) {
    var commentCode = '';
    // Only collect comments for blocks that aren't inline.
    if (!block.outputConnection || !block.outputConnection.targetConnection) {
      // Collect comment for this block.
      var comment = block.getCommentText();
      comment = Blockly.utils.wrap(comment, Blockly.UR_script.COMMENT_WRAP - 3);
      if (comment) {
        if (block.getProcedureDef) {
          // Use a comment block for function comments.
          commentCode += '"""' + comment + '\n"""\n';
        } else {
          commentCode += Blockly.UR_script.prefixLines(comment + '\n', '# ');
        }
      }
      // Collect comments for all value arguments.
      // Don't collect comments for nested statements.
      for (var i = 0; i < block.inputList.length; i++) {
        if (block.inputList[i].type == Blockly.INPUT_VALUE) {
          var childBlock = block.inputList[i].connection.targetBlock();
          if (childBlock) {
            var comment = Blockly.UR_script.allNestedComments(childBlock);
            if (comment) {
              commentCode += Blockly.UR_script.prefixLines(comment, '# ');
            }
          }
        }
      }
    }
    var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
    var nextCode = Blockly.UR_script.blockToCode(nextBlock);
    return commentCode + code + nextCode;
  };
  
  Blockly.UR_script.getAdjustedInt = function(block, atId, opt_delta, opt_negate) {
    var delta = opt_delta || 0;
    if (block.workspace.options.oneBasedIndex) {
      delta--;
    }
    var defaultAtIndex = block.workspace.options.oneBasedIndex ? '1' : '0';
    var atOrder = delta ? Blockly.UR_script.ORDER_ADDITIVE :
        Blockly.UR_script.ORDER_NONE;
    var at = Blockly.UR_script.valueToCode(block, atId, atOrder) || defaultAtIndex;
  
    if (Blockly.isNumber(at)) {
      // If the index is a naked number, adjust it right now.
      at = parseInt(at, 10) + delta;
      if (opt_negate) {
        at = -at;
      }
    } else {
      // If the index is dynamic, adjust it in code.
      if (delta > 0) {
        at = 'int(' + at + ' + ' + delta + ')';
      } else if (delta < 0) {
        at = 'int(' + at + ' - ' + -delta + ')';
      } else {
        at = 'int(' + at + ')';
      }
      if (opt_negate) {
        at = '-' + at;
      }
    }
    return at;
  };
  