/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating UR_script for math blocks.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.UR_script.math');
goog.provide('Blockly.Constants.Math');

goog.require('Blockly.UR_script');


// If any new block imports any library, add that library name here.
//Blockly.UR_script.addReservedWords('math,random,Number');

Blockly.UR_script['math_number'] = function(block) {
  // Numeric value.
  var code = parseFloat(block.getFieldValue('NUM'));
  return [code, Blockly.UR_script.ORDER_ATOMIC];
};

Blockly.UR_script['math_arithmetic'] = function(block) {
  // Basic arithmetic operators, and power.
  var OPERATORS = {
    'ADD': [' + ', Blockly.UR_script.ORDER_ADDITIVE],
    'MINUS': [' - ', Blockly.UR_script.ORDER_ADDITIVE],
    'MULTIPLY': [' * ', Blockly.UR_script.ORDER_MULTIPLICATIVE],
    'DIVIDE': [' / ', Blockly.UR_script.ORDER_MULTIPLICATIVE],
    'POWER': [' ** ', Blockly.UR_script.ORDER_EXPONENTIATION]
  };
  var tuple = OPERATORS[block.getFieldValue('OP')];
  var operator = tuple[0];
  var order = tuple[1];
  var argument0 = Blockly.UR_script.valueToCode(block, 'A', order) || '0';
  var argument1 = Blockly.UR_script.valueToCode(block, 'B', order) || '0';
  var code = argument0 + operator + argument1;
  return [code, order];
  // In case of 'DIVIDE', division between integers returns different results
  // in UR_script 2 and 3. However, is not an issue since Blockly does not
  // guarantee identical results in all languages.  To do otherwise would
  // require every operator to be wrapped in a function call.  This would kill
  // legibility of the generated code.
};

Blockly.UR_script['math_change'] = function(block) {
  // Add to a variable in place.
  Blockly.UR_script.definitions_['from_numbers_import_Number'] =
      'from numbers import Number';
  var argument0 = Blockly.UR_script.valueToCode(block, 'DELTA',
      Blockly.UR_script.ORDER_ADDITIVE) || '0';
  var varName = Blockly.UR_script.variableDB_.getName(block.getFieldValue('VAR'),
      Blockly.Variables.NAME_TYPE);
  return varName + ' = ' + varName + ' + ' + argument0 + '\n';
};