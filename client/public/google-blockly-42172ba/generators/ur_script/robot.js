'use strict';

goog.provide('Blockly.UR_script.robot');

goog.require('Blockly.UR_script');

Blockly.UR_script['main_block'] = function(block) {
    //var text_functionname = block.getFieldValue('FunctionName');
    //var statements_stack = Blockly.UR_script.statementToCode(block, 'STACK');

    var funcName = block.getFieldValue('FunctionName');
    var defFunc = 'def '+ funcName + '():\n';
    var endFunc = '\nend\n';
    var branch = Blockly.UR_script.statementToCode(block, 'STACK')

    var code = defFunc + branch + endFunc;

    return code;
};

Blockly.UR_script['movej'] = function(block) {

    var q1 = block.getFieldValue('Q1');
    var q2 = block.getFieldValue('Q2');
    var q3 = block.getFieldValue('Q3');
    var q4 = block.getFieldValue('Q4');
    var q5 = block.getFieldValue('Q5');
    var q6 = block.getFieldValue('Q6');
    var a = block.getFieldValue('A');
    var v = block.getFieldValue('V');

    var arr = '['+q1+', '+q2+', '+q3+', '+q4+', '+q5+', '+q6+']';
    var code = 'movej('+arr+', '+a+', '+v+')\n';
    return code;

};

Blockly.UR_script['sleep'] = function(block) {
    var second = block.getFieldValue('SLEEP');
    // TODO: Assemble UR_script into code variable.
    var code = 'sleep('+second+')\n';
    return code;
};

Blockly.UR_script['position'] = function(block) {
    var x = Blockly.UR_script.valueToCode(block, 'X', Blockly.UR_script.ORDER_ATOMIC);
    var y = Blockly.UR_script.valueToCode(block, 'Y', Blockly.UR_script.ORDER_ATOMIC);
    var z = Blockly.UR_script.valueToCode(block, 'Z', Blockly.UR_script.ORDER_ATOMIC);
    var rx = Blockly.UR_script.valueToCode(block, 'RX', Blockly.UR_script.ORDER_ATOMIC);
    var ry = Blockly.UR_script.valueToCode(block, 'RY', Blockly.UR_script.ORDER_ATOMIC);
    var rz = Blockly.UR_script.valueToCode(block, 'RZ', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = 'p['+x+', '+y+', '+z+', '+rx+', '+ry+', '+rz+']';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.UR_script.ORDER_ATOMIC];
};

Blockly.UR_script['rotation'] = function(block) {
    var q1 = Blockly.UR_script.valueToCode(block, 'Q1', Blockly.UR_script.ORDER_ATOMIC);
    var q2 = Blockly.UR_script.valueToCode(block, 'Q2', Blockly.UR_script.ORDER_ATOMIC);
    var q3 = Blockly.UR_script.valueToCode(block, 'Q3', Blockly.UR_script.ORDER_ATOMIC);
    var q4 = Blockly.UR_script.valueToCode(block, 'Q4', Blockly.UR_script.ORDER_ATOMIC);
    var q5 = Blockly.UR_script.valueToCode(block, 'Q5', Blockly.UR_script.ORDER_ATOMIC);
    var q6 = Blockly.UR_script.valueToCode(block, 'Q6', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = '['+q1+', '+q2+', '+q3+', '+q4+', '+q5+', '+q6+']';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.UR_script.ORDER_ATOMIC];
};

Blockly.UR_script['move'] = function(block) {
    var move_type = block.getFieldValue('MOVE_TYPE');
    var pose_to = Blockly.UR_script.valueToCode(block, 'POSE_TO', Blockly.UR_script.ORDER_ATOMIC);
    var pose_via = Blockly.UR_script.valueToCode(block, 'POSE_VIA', Blockly.UR_script.ORDER_ATOMIC);
    var a = Blockly.UR_script.valueToCode(block, 'A', Blockly.UR_script.ORDER_ATOMIC);
    var v = Blockly.UR_script.valueToCode(block, 'V', Blockly.UR_script.ORDER_ATOMIC);
    var t = Blockly.UR_script.valueToCode(block, 'T', Blockly.UR_script.ORDER_ATOMIC);
    var r = Blockly.UR_script.valueToCode(block, 'R', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = '';
    var move = '';

    //determine move type
    ('CIRCULAR' == move_type) ? move = 'movec' :
    ('JOINT_SPACE' == move_type) ? move = 'movej' : move = 'movel';

    if ('movec' == move) {
        code = move + '(' + pose_to + ', '+ pose_via +
        ', a=' + a + ', v=' + v + ', t=' + t + ', r=' + r + ')\n';
    } else {
        code = move + '(' + pose_to +
        ', a=' + a + ', v=' + v + ', t=' + t + ', r=' + r + ')\n';
    }

    return code;
  };

Blockly.UR_script['move_joint'] = function(block) {
    var dropdown_abs_rel = block.getFieldValue('ABS_REL');
    var value_a = Blockly.UR_script.valueToCode(block, 'ANG_ACCELERATION', Blockly.UR_script.ORDER_ATOMIC);
    var value_v = Blockly.UR_script.valueToCode(block, 'ANG_VELOCITY', Blockly.UR_script.ORDER_ATOMIC);
    var value_t = Blockly.UR_script.valueToCode(block, 'TIME', Blockly.UR_script.ORDER_ATOMIC);
    var value_r = Blockly.UR_script.valueToCode(block, 'RADIUS', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = '';
    var code_pre_calc = '';

    if (this.getInputTargetBlock('JPOSE') !== null &&
        this.getInputTargetBlock('JPOSE').type === 'joint_pose') {

        if (dropdown_abs_rel == "ABSOLUTE") {
            var value_jpose = Blockly.UR_script.valueToCode(block, 'JPOSE', Blockly.UR_script.ORDER_ATOMIC);
            code = value_jpose;
        } else if (dropdown_abs_rel == "RELATIVE") {
            var child = this.getInputTargetBlock('JPOSE');
            var j1 = 0, j2 = 0, j3 = 0, j4 = 0, j5 = 0, j6 = 0;
            if (child !== null) {
                j1 = Blockly.UR_script.valueToCode(child, 'J1', Blockly.UR_script.ORDER_ATOMIC);
                j2 = Blockly.UR_script.valueToCode(child, 'J2', Blockly.UR_script.ORDER_ATOMIC);
                j3 = Blockly.UR_script.valueToCode(child, 'J3', Blockly.UR_script.ORDER_ATOMIC);
                j4 = Blockly.UR_script.valueToCode(child, 'J4', Blockly.UR_script.ORDER_ATOMIC);
                j5 = Blockly.UR_script.valueToCode(child, 'J5', Blockly.UR_script.ORDER_ATOMIC);
                j6 = Blockly.UR_script.valueToCode(child, 'J6', Blockly.UR_script.ORDER_ATOMIC);
            }
            code_pre_calc = 'arr = get_actual_joint_positions()\n';

            code = `[arr[0] + ${j1}, arr[1] + ${j2}, arr[2] + ${j3}, arr[3] + ${j4}, arr[4] + ${j5}, arr[5] + ${j6}]`;
        }
    } else if (this.getInputTargetBlock('JPOSE') !== null) {
        var value_jpose = Blockly.UR_script.blockToCode(this.getInputTargetBlock('JPOSE'));
        if (dropdown_abs_rel == "ABSOLUTE") {
            code = value_jpose;
        } else if (dropdown_abs_rel == "RELATIVE") {
            var value_jpose_arrary = value_jpose.replace(/[\[\] ]/g,'').trim().split(',');
            var j1 = 0, j2 = 0, j3 = 0, j4 = 0, j5 = 0, j6 = 0;
            if (child !== null) {
                j1 = value_jpose_arrary[0];
                j2 = value_jpose_arrary[1];
                j3 = value_jpose_arrary[2];
                j4 = value_jpose_arrary[3];
                j5 = value_jpose_arrary[4];
                j6 = value_jpose_arrary[5];
            }
            code_pre_calc = 'arr = get_actual_joint_positions()\n';
            code = `[arr[0] + ${j1}, arr[1] + ${j2}, arr[2] + ${j3}, arr[3] + ${j4}, arr[4] + ${j5}, arr[5] + ${j6}]`;
        }
    }

    if (code === '')
    {
       return '';
    }

    code =  code_pre_calc + 'movej(' + code;
    if (value_a != '') {code += (', a='+value_a);}
    if (value_v != '') {code += (', v='+value_v);}
    if (value_t != '') {code += (', t='+value_t);}
    if (value_r != '') {code += (', r='+value_r);}
    code += ')';

    return code + '\n';
};

Blockly.UR_script['move_cart'] = function(block) {
    var dropdown_abs_rel = block.getFieldValue('ABS_REL');
    var value_a = Blockly.UR_script.valueToCode(block, 'ACCELERATION', Blockly.UR_script.ORDER_ATOMIC);
    var value_v = Blockly.UR_script.valueToCode(block, 'VELOCITY', Blockly.UR_script.ORDER_ATOMIC);
    var value_t = Blockly.UR_script.valueToCode(block, 'TIME', Blockly.UR_script.ORDER_ATOMIC);
    var value_r = Blockly.UR_script.valueToCode(block, 'RADIUS', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = '';

    if (this.getInputTargetBlock('CART-POSE') !== null &&
        this.getInputTargetBlock('CART-POSE').type === 'cart_pose') {

        if (dropdown_abs_rel == "ABSOLUTE") {
            var value_cart_pose = Blockly.UR_script.valueToCode(block, 'CART-POSE', Blockly.UR_script.ORDER_ATOMIC);
            code = value_cart_pose;
        } else if (dropdown_abs_rel == "RELATIVE") {
            var child = this.getInputTargetBlock('CART-POSE');
            var x = 0, y = 0, z = 0, rx = 0, ry = 0, rz = 0;
            if (child !== null) {
                x = Blockly.UR_script.valueToCode(child, 'X', Blockly.UR_script.ORDER_ATOMIC);
                y = Blockly.UR_script.valueToCode(child, 'Y', Blockly.UR_script.ORDER_ATOMIC);
                z = Blockly.UR_script.valueToCode(child, 'Z', Blockly.UR_script.ORDER_ATOMIC);
                rx = Blockly.UR_script.valueToCode(child, 'RX', Blockly.UR_script.ORDER_ATOMIC);
                ry = Blockly.UR_script.valueToCode(child, 'RY', Blockly.UR_script.ORDER_ATOMIC);
                rz = Blockly.UR_script.valueToCode(child, 'RZ', Blockly.UR_script.ORDER_ATOMIC);
            }
            code = 'pose_add(get_actual_tcp_pose(), p['+x+', '+y+', '+z+', '+rx+', '+ry+', '+rz+'])';
        }
    } else if (this.getInputTargetBlock('CART-POSE') !== null) {
        var value_cart_pose = Blockly.UR_script.blockToCode(this.getInputTargetBlock('CART-POSE'));
        if (dropdown_abs_rel == "ABSOLUTE") {
            code = value_cart_pose;
        } else if (dropdown_abs_rel == "RELATIVE") {
            code = 'pose_add(get_actual_tcp_pose(), '+value_cart_pose+')';
        }
    }

    if (code === '')
    {
       return '';
    }

    code = 'movel(' + code;
    if (value_a != '') {code += (', a='+value_a);}
    if (value_v != '') {code += (', v='+value_v);}
    if (value_t != '') {code += (', t='+value_t);}
    if (value_r != '') {code += (', r='+value_r);}
    code += ')';

    return code + '\n';
};

Blockly.UR_script['3D_vector'] = function (block) {
    var value_x = Blockly.UR_script.valueToCode(block, 'X', Blockly.UR_script.ORDER_ATOMIC);
    var value_y = Blockly.UR_script.valueToCode(block, 'Y', Blockly.UR_script.ORDER_ATOMIC);
    var value_z = Blockly.UR_script.valueToCode(block, 'Z', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = `[${value_x}, ${value_y}, ${value_z}]`;
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.UR_script.ORDER_ATOMIC];
};

Blockly.UR_script['define_tool'] = function (block) {
    var value_tcp_pose = Blockly.UR_script.valueToCode(block, 'tcp_pose', Blockly.UR_script.ORDER_ATOMIC);
    var value_payload_mass = Blockly.UR_script.valueToCode(block, 'payload_mass', Blockly.UR_script.ORDER_ATOMIC);
    var value_payload_cog = Blockly.UR_script.valueToCode(block, 'payload_cog', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.

    var setTcp = 'set_tcp(' + value_tcp_pose + ')' + '\n';
    var setPayload = 'set_payload(' + value_payload_mass + ', ' + value_payload_cog + ')' + '\n';

    var code = setTcp + setPayload;

    return code;
};

Blockly.UR_script['use_tool'] = function (block) {
    var value_tool = Blockly.UR_script.blockToCode(this.getInputTargetBlock('TOOL'));
    // TODO: Assemble UR_script into code variable.
    var code = value_tool;
    return code;
};

Blockly.UR_script['cart_pose'] = function(block) {
    var x = Blockly.UR_script.valueToCode(block, 'X', Blockly.UR_script.ORDER_ATOMIC);
    var y = Blockly.UR_script.valueToCode(block, 'Y', Blockly.UR_script.ORDER_ATOMIC);
    var z = Blockly.UR_script.valueToCode(block, 'Z', Blockly.UR_script.ORDER_ATOMIC);
    var rx = Blockly.UR_script.valueToCode(block, 'RX', Blockly.UR_script.ORDER_ATOMIC);
    var ry = Blockly.UR_script.valueToCode(block, 'RY', Blockly.UR_script.ORDER_ATOMIC);
    var rz = Blockly.UR_script.valueToCode(block, 'RZ', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = 'p['+x+', '+y+', '+z+', '+rx+', '+ry+', '+rz+']';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.UR_script.ORDER_ATOMIC];
  };

Blockly.UR_script['joint_pose'] = function(block) {
    var j1 = Blockly.UR_script.valueToCode(block, 'J1', Blockly.UR_script.ORDER_ATOMIC);
    var j2 = Blockly.UR_script.valueToCode(block, 'J2', Blockly.UR_script.ORDER_ATOMIC);
    var j3 = Blockly.UR_script.valueToCode(block, 'J3', Blockly.UR_script.ORDER_ATOMIC);
    var j4 = Blockly.UR_script.valueToCode(block, 'J4', Blockly.UR_script.ORDER_ATOMIC);
    var j5 = Blockly.UR_script.valueToCode(block, 'J5', Blockly.UR_script.ORDER_ATOMIC);
    var j6 = Blockly.UR_script.valueToCode(block, 'J6', Blockly.UR_script.ORDER_ATOMIC);
    // TODO: Assemble UR_script into code variable.
    var code = '['+j1+', '+j2+', '+j3+', '+j4+', '+j5+', '+j6+']';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.UR_script.ORDER_ATOMIC];
  };

Blockly.UR_script['get_actual_tcp_pose'] = function(block) {
    // TODO: Assemble UR_script into code variable.
    var code = 'get_actual_tcp_pose()';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.UR_script.ORDER_NONE];
  };
