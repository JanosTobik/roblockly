export const projectToolbox = `
<xml id="toolbox" style="display: none">
  <category name="Logic" colour="%{BKY_LOGIC_HUE}">
    <block type="controls_if"></block>
    <block type="logic_boolean"></block>
    <block type="logic_compare"></block>
    <block type="logic_operation"></block>
    <block type="logic_negate"></block>
    <block type="logic_null"></block>
  </category>
  <category name="Loops" colour="%{BKY_LOOPS_HUE}">
    <block type="controls_whileUntil"></block>
  </category>
  <category name="Variables" colour="%{BKY_VARIABLES_HUE}" custom="VARIABLE">
  </category>
  <category name="Math" colour="%{BKY_MATH_HUE}">
    <block type="math_number"></block>
    <block type="math_arithmetic"></block>
    <block type="math_change"></block>
  </category>
  <category name="Text" colour="%{BKY_TEXTS_HUE}">
    <block type="text"></block>
  </category>
  <category name="Lists" colour="%{BKY_LISTS_HUE}">
    <block type="lists_create_empty"></block>
    <block type="lists_create_with"></block>
    <block type="lists_getIndex"></block>
    <block type="lists_setIndex"></block>
  </category>
  <category name="Robot" colour="193">
    <block type="move_joint"></block>
    <block type="move_cart"></block>
    <block type="cart_pose">
      <value name="X"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Y"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Z"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="RX"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="RY"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="RZ"><block type="math_number"><field name="NUM">0</field></block></value>
    </block>
    <block type="joint_pose">
      <value name="J1"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J2"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J3"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J4"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J5"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J6"><block type="math_number"><field name="NUM">0</field></block></value>
    </block>
    <block type="get_actual_tcp_pose"></block>
    <block type="define_tool"></block>
    <block type="3D_vector">
      <value name="X"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Y"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Z"><block type="math_number"><field name="NUM">0</field></block></value>
    </block>
    <block type="use_tool"></block>
    <block type="sleep"></block>
  </category>
  <category name="Routine" colour="345" custom="ROUTINE" ></category>
  <category name="Pose" colour="255" custom="POSE"></category>
  <category name="Tool" colour="40" custom="TOOL"></category>
  <category name="Other" colour="230">
    <block type="main_block"></block>
  </category>

</xml>
`;

export const routineToolbox = `
<xml id="toolbox" style="display: none">
  <category name="Logic" colour="%{BKY_LOGIC_HUE}">
    <block type="controls_if"></block>
    <block type="logic_boolean"></block>
    <block type="logic_compare"></block>
    <block type="logic_operation"></block>
    <block type="logic_negate"></block>
    <block type="logic_null"></block>
  </category>
  <category name="Loops" colour="%{BKY_LOOPS_HUE}">
    <block type="controls_whileUntil"></block>
  </category>
  <category name="Variables" colour="%{BKY_VARIABLES_HUE}" custom="VARIABLE">
  </category>
  <category name="Math" colour="%{BKY_MATH_HUE}">
    <block type="math_number"></block>
    <block type="math_arithmetic"></block>
    <block type="math_change"></block>
  </category>
  <category name="Text" colour="%{BKY_TEXTS_HUE}">
    <block type="text"></block>
  </category>
  <category name="Lists" colour="%{BKY_LISTS_HUE}">
    <block type="lists_create_empty"></block>
    <block type="lists_create_with"></block>
    <block type="lists_getIndex"></block>
    <block type="lists_setIndex"></block>
  </category>
  <category name="Robot" colour="193">
    <block type="move_joint"></block>
    <block type="move_cart"></block>
    <block type="cart_pose">
      <value name="X"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Y"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Z"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="RX"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="RY"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="RZ"><block type="math_number"><field name="NUM">0</field></block></value>
    </block>
    <block type="joint_pose">
      <value name="J1"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J2"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J3"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J4"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J5"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="J6"><block type="math_number"><field name="NUM">0</field></block></value>
    </block>
    <block type="get_actual_tcp_pose"></block>
    <block type="define_tool"></block>
    <block type="3D_vector">
      <value name="X"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Y"><block type="math_number"><field name="NUM">0</field></block></value>
      <value name="Z"><block type="math_number"><field name="NUM">0</field></block></value>
    </block>
    <block type="use_tool"></block>
    <block type="sleep"></block>
  </category>
  <category name="Pose" colour="255" custom="POSE"></category>
  <category name="Tool" colour="40" custom="TOOL"></category>
</xml>
`;
