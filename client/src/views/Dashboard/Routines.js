import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

import { Card, CardBody, CardHeader, Row, Col } from 'reactstrap';

import { RoutineDialog, RoutineDeleteDialog } from '../../components';
import { userActions } from '../../actions';
import { history } from '../../helpers';

class Routines extends Component {
  constructor(props) {
    super(props);

    this.state = {
      routines: null,
    }

    this.routineDialog = null;
    this.routineDeleteDialog = null;
  }

  componentDidMount() {
    this.updateTable();
  }

  updateTable() {
    const { dispatch, user } = this.props;
    dispatch(userActions.getRoutines(user.id))
    .subscribe(response => { this.setState({ routines: response.data }) });
  }

  onClickAddNew() {
    this.routineDialog.setState({
      isDialogVisible: true,
      isNewElement: true
    });
  }

  onClickDelete(e) {
    this.routineDeleteDialog.setState({
      isDialogVisible: true,
      routine: e
    })
  }

  onClickEdit(e) {
    this.routineDialog.setState({
      isDialogVisible: true,
      isNewElement: false,
      routine: e
    });
  }

  onClickOpen(e) {
    localStorage.setItem('selectedRoutine', JSON.stringify(e));
    var sid = localStorage.getItem('rrsmSid') ? JSON.parse(localStorage.getItem('rrsmSid')).sid : undefined;
    history.push(`/${sid}/routines/${e.routineName}`);
  }

  actionTemplate(rowData, column) {
    return <div>
      <Button onClick={() => this.onClickOpen(rowData)} icon="pi pi-external-link" className="p-button-rounded common-button" style={{ marginRight: '.5em' }} tooltip="Open routine" tooltipOptions={{ position: 'bottom' }} ></Button>
      <Button onClick={() => this.onClickEdit(rowData)} icon="pi pi-pencil" className="p-button-rounded warning-button" style={{ marginRight: '.5em' }} tooltip="Edit routine" tooltipOptions={{ position: 'bottom' }} ></Button>
      <Button onClick={() => this.onClickDelete(rowData)} icon="pi pi-trash" className="p-button-rounded danger-button" tooltip="Delete routine" tooltipOptions={{ position: 'bottom' }} ></Button>
    </div>
  }

  render() {
    var footer =
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button className="common-button" style={{ float: 'left' }} label="New" icon="pi pi-plus" onClick={this.onClickAddNew.bind(this)} />
      </div>;

    const { routines } = this.state;
    if (routines) {
      routines.forEach(element => {
        element['created'] = new Date(element['created']).toLocaleString();

        if (element['modified'] !== null) {
          element['modified'] = new Date(element['modified']).toLocaleString();
        }
      });
    }
    return (
        <div className="animated fadeIn">
            <Row className="justify-content-center">
                <Col sm={10}>
                    <Card>
                        <CardHeader>
                            <i className="fa fa-list-ul"></i><span> Routines</span>
                        </CardHeader>
                        <CardBody>
                            <DataTable value={routines} footer={footer} responsive={true}
                                paginator={true} rows={5} rowsPerPageOptions={[5, 10, 20]} >
                                <Column field="routineName" header="Name" style={{ textAlign: 'center', width: '13em' }} />
                                <Column field="description" header="Description" style={{ textAlign: 'center', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }} />
                                <Column field="created" header="Created" style={{ textAlign: 'center', width: '12em' }} />
                                <Column field="modified" header="Modified" style={{ textAlign: 'center', width: '12em' }} />
                                <Column body={this.actionTemplate.bind(this)} style={{ textAlign: 'center', width: '10em' }} />
                            </DataTable>
                        </CardBody>

                        <RoutineDialog ref={el => this.routineDialog = el} updateTable={() => this.updateTable()}></RoutineDialog>

                        <RoutineDeleteDialog ref={el => this.routineDeleteDialog = el} updateTable={() => this.updateTable()}></RoutineDeleteDialog>

                    </Card>
                </Col>
            </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert, authentication, routine } = state;
  const { routines } = state.user;
  const { user } = authentication;
  return {
    alert,
    user,
    routines,
    routine
  };
}

const RoutinesPage = connect(mapStateToProps)(Routines);
export default RoutinesPage;
