import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

import { Card, CardBody, CardHeader, Row, Col } from 'reactstrap';

import { ProjectDialog, ProjectDeleteDialog } from '../../components';
import { userActions } from '../../actions';
import { history } from '../../helpers';

class Projects extends Component {
  constructor(props) {
    super(props);

    this.state = {
      projects: null,
    }

    this.projectDialog = null;
    this.projectDeleteDialog = null;
  }

  componentDidMount() {
    this.updateTable();
  }

  updateTable() {
    const { dispatch, user } = this.props;
    dispatch(userActions.getProjects(user.id))
    .subscribe(response => { this.setState({ projects: response.data }) });
  }

  onClickAddNew() {
    this.projectDialog.setState({
      isDialogVisible: true,
      isNewElement: true
    });
  }

  onClickDelete(e) {
    this.projectDeleteDialog.setState({
      isDialogVisible: true,
      project: e
    })
  }

  onClickEdit(e) {
    this.projectDialog.setState({
      isDialogVisible: true,
      isNewElement: false,
      project: e
    });
  }

  onClickOpen(e) {
    localStorage.setItem('selectedProject', JSON.stringify(e));
    var sid = localStorage.getItem('rrsmSid') ? JSON.parse(localStorage.getItem('rrsmSid')).sid : undefined;
    history.push(`/${sid}/projects/${e.projectName}`);
  }

  actionTemplate(rowData, column) {
    return <div>
      <Button onClick={() => this.onClickOpen(rowData)} icon="pi pi-external-link" className="p-button-rounded common-button" style={{ marginRight: '.5em' }} tooltip="Open project" tooltipOptions={{ position: 'bottom' }} ></Button>
      <Button onClick={() => this.onClickEdit(rowData)} icon="pi pi-pencil" className="p-button-rounded warning-button" style={{ marginRight: '.5em' }} tooltip="Edit project" tooltipOptions={{ position: 'bottom' }} ></Button>
      <Button onClick={() => this.onClickDelete(rowData)} icon="pi pi-trash" className="p-button-rounded danger-button" tooltip="Delete project" tooltipOptions={{ position: 'bottom' }} ></Button>
    </div>
  }

  render() {
    var footer =
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button className="common-button" style={{ float: 'left' }} label="New" icon="pi pi-plus" onClick={this.onClickAddNew.bind(this)} />
      </div>;

    const { projects } = this.state;
    if (projects) {
      projects.forEach(element => {
        element['created'] = new Date(element['created']).toLocaleString();

        if (element['modified'] !== null) {
          element['modified'] = new Date(element['modified']).toLocaleString();
        }
      });
    }
    return (
      <div className="animated fadeIn">
        <Row className="justify-content-center">
          <Col sm={10}>
            <Card>
              <CardHeader>
                <i className="fa fa-list-ul"></i><span> Projects</span>
              </CardHeader>
              <CardBody>
                <DataTable value={projects} footer={footer} responsive={true}
                  paginator={true} rows={5} rowsPerPageOptions={[5, 10, 20]} >
                  <Column field="projectName" header="Name" style={{ textAlign: 'center', width: '13em' }}  />
                  <Column field="description" header="Description" style={{textAlign: 'center', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis'}} />
                  <Column field="created" header="Created" style={{ textAlign: 'center', width: '12em' }} />
                  <Column field="modified" header="Modified" style={{ textAlign: 'center', width: '12em' }} />
                  <Column body={this.actionTemplate.bind(this)} style={{ textAlign: 'center', width: '10em' }} />
                </DataTable>
              </CardBody>

              <ProjectDialog ref={el => this.projectDialog = el} updateTable={() => this.updateTable()}></ProjectDialog>

              <ProjectDeleteDialog ref={el => this.projectDeleteDialog = el} updateTable={() => this.updateTable()}></ProjectDeleteDialog>

            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert, authentication, project } = state;
  const { projects } = state.user;
  const { user } = authentication;
  return {
    alert,
    user,
    projects,
    project
  };
}

const ProjectsPage = connect(mapStateToProps)(Projects);
export default ProjectsPage;
