import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

import { Card, CardBody, CardHeader, Row, Col } from 'reactstrap';

import { ToolDialog, ToolDeleteDialog } from '../../components';
import { userActions } from '../../actions';

class Tools extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tools: null,
        }

        this.toolDialog = null;
        this.toolDeleteDialog = null;
    }

    componentDidMount() {
        this.updateTable();
    }

    updateTable() {
        const { dispatch, user } = this.props;
        dispatch(userActions.getTools(user.id))
        .subscribe(response => { this.setState({ tools: response.data }) });
    }

    onClickAddNew() {
        this.toolDialog.setState({
            isDialogVisible: true,
            isNewElement: true,
        })
    }

    onClickDelete(e) {
        this.toolDeleteDialog.setState({
            isDialogVisible: true,
            tool: e
        })
    }

    onClickEdit(e) {
        const tcpPose = JSON.parse(e.tcpPose);
        const payloadCog = JSON.parse(e.payloadCog);

        this.tcpXX = tcpPose[0]; this.tcpYY = tcpPose[1]; this.tcpZZ = tcpPose[2];
        this.tcpRX = tcpPose[3]; this.tcpRY = tcpPose[4]; this.tcpRZ = tcpPose[5];
        this.plCogX = payloadCog[0]; this.plCogY = payloadCog[1]; this.plCogZ = payloadCog[2];
        this.toolDialog.setState({
            isDialogVisible: true,
            isNewElement: false,
            tool: e,
            tcpXX: tcpPose[0], tcpYY: tcpPose[1], tcpZZ: tcpPose[2],
            tcpRX: tcpPose[3], tcpRY: tcpPose[4], tcpRZ: tcpPose[5],
            plCogX: payloadCog[0], plCogY: payloadCog[1], plCogZ: payloadCog[2],
        });
    }

    actionTemplate(rowData, column) {
        return <div>
            <Button onClick={() => this.onClickEdit(rowData)} icon="pi pi-pencil" className="p-button-rounded warning-button" style={{ marginRight: '.5em' }} tooltip="Edit tool" tooltipOptions={{ position: 'bottom' }} ></Button>
            <Button onClick={() => this.onClickDelete(rowData)} icon="pi pi-trash" className="p-button-rounded danger-button" tooltip="Delete tool" tooltipOptions={{ position: 'bottom' }} ></Button>
        </div>
    }

  render() {
    const { tools } = this.state;
    if (tools) {
        tools.forEach(element => {
            element['created'] = new Date(element['created']).toLocaleString();
            if (element['modified'] !== null) {
                element['modified'] = new Date(element['modified']).toLocaleString();
            }
      });
    }
    var footer =
        <div className="p-clearfix" style={{ width: '100%' }}>
            <Button className="common-button" style={{ float: 'left' }} label="New" icon="pi pi-plus" onClick={this.onClickAddNew.bind(this)} />
        </div>;
    return (
        <div className="animated fadeIn">
            <Row className="justify-content-center">
                <Col sm={10}>
                    <Card>
                        <CardHeader>
                            <i className="fa fa-list-ul"></i><span> Tools</span>
                        </CardHeader>
                        <CardBody>
                            <DataTable value={tools} responsive={true} footer={footer}
                                paginator={true} rows={5} rowsPerPageOptions={[5, 10, 20]} >
                                <Column field="toolName" header="Name" style={{ textAlign: 'center', width: '13em' }} />
                                <Column field="description" header="Description" style={{ textAlign: 'center', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }} />
                                <Column field="created" header="Created" style={{ textAlign: 'center', width: '12em' }} />
                                <Column field="modified" header="Modified" style={{ textAlign: 'center', width: '12em' }} />
                                <Column body={this.actionTemplate.bind(this)} style={{ textAlign: 'center', width: '10em' }} />
                            </DataTable>
                        </CardBody>

                        <ToolDialog ref={el => this.toolDialog = el} updateTable={() => this.updateTable()}></ToolDialog>

                        <ToolDeleteDialog ref={el => this.toolDeleteDialog = el} updateTable={() => this.updateTable()}></ToolDeleteDialog>

                    </Card>
                </Col>
            </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
    const { alert, authentication, tool } = state;
    const { tools } = state.user;
    const { user } = authentication;
    return {
        alert,
        user,
        tools,
        tool
    };
}

const ToolsPage = connect(mapStateToProps)(Tools);
export default ToolsPage;
