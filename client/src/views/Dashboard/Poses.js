import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

import { Card, CardBody, CardHeader, Row, Col } from 'reactstrap';

import { PoseDialog, PoseDeleteDialog } from '../../components';
import { userActions } from '../../actions';

class Poses extends Component {
  constructor(props) {
    super(props);

    this.state = {
      poses: null,
    }

    this.poseDialog = null;
    this.poseDeleteDialog = null;
  }

  componentDidMount() {
    this.updateTable();
  }

  updateTable() {
    const { dispatch, user } = this.props;
    dispatch(userActions.getPoses(user.id))
    .subscribe(response => { this.setState({ poses: response.data }) });
  }

  onClickDelete(e) {
    this.poseDeleteDialog.setState({
      isDialogVisible: true,
      pose: e
    })
  }

  onClickEdit(e) {
    this.poseDialog.setState({
      isDialogVisible: true,
      isNewElement: false,
      pose: e
    });
  }

  actionTemplate(rowData, column) {
    return <div>
      <Button onClick={() => this.onClickEdit(rowData)} icon="pi pi-pencil" className="p-button-rounded warning-button" style={{ marginRight: '.5em' }} tooltip="Edit pose" tooltipOptions={{ position: 'bottom' }} ></Button>
      <Button onClick={() => this.onClickDelete(rowData)} icon="pi pi-trash" className="p-button-rounded danger-button" tooltip="Delete pose" tooltipOptions={{ position: 'bottom' }} ></Button>
    </div>
  }

  render() {
    const { poses } = this.state;
    if (poses) {
        poses.forEach(element => {
            element['created'] = new Date(element['created']).toLocaleString();
            if (element['modified'] !== null) {
                element['modified'] = new Date(element['modified']).toLocaleString();
            }
            if (element['poseType'] === 'cart-type') {
              element['poseType'] = 'cartesian';
            } else if (element['poseType'] === 'joint-type') {
              element['poseType'] = 'joint';
            }
      });
    }
    return (
        <div className="animated fadeIn">
            <Row className="justify-content-center">
                <Col sm={10}>
                    <Card>
                        <CardHeader>
                            <i className="fa fa-list-ul"></i><span> Poses</span>
                        </CardHeader>
                        <CardBody>
                            <DataTable value={poses} responsive={true}
                                paginator={true} rows={5} rowsPerPageOptions={[5, 10, 20]} >
                                <Column field="poseName" header="Name" style={{ textAlign: 'center', width: '13em' }} />
                                <Column field="poseType" header="Type" style={{ textAlign: 'center', width: '7em' }} />
                                <Column field="description" header="Description" style={{ textAlign: 'center', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }} />
                                <Column field="created" header="Created" style={{ textAlign: 'center', width: '12em' }} />
                                <Column field="modified" header="Modified" style={{ textAlign: 'center', width: '12em' }} />
                                <Column body={this.actionTemplate.bind(this)} style={{ textAlign: 'center', width: '10em' }} />
                            </DataTable>
                        </CardBody>

                        <PoseDialog ref={el => this.poseDialog = el} updateTable={() => this.updateTable()}></PoseDialog>

                        <PoseDeleteDialog ref={el => this.poseDeleteDialog = el} updateTable={() => this.updateTable()}></PoseDeleteDialog>

                    </Card>
                </Col>
            </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert, authentication, pose } = state;
  const { poses } = state.user;
  const { user } = authentication;
  return {
    alert,
    user,
    poses,
    pose
  };
}

const PosesPage = connect(mapStateToProps)(Poses);
export default PosesPage;
