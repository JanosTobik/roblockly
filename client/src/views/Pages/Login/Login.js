import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { flatMap } from 'rxjs/operators';

import { AppFooter } from '@coreui/react';
import { Button, Col, Container, Form, FormFeedback, FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Growl } from 'primereact/growl';

import { history } from '../../../helpers';
import { userActions } from '../../../actions';
import { rrsmService } from '../../../services';

import roblocklyIcon from '../../../assets/img/design/01_roblockly_logo_white.svg';
import userIcon from '../../../assets/img/design/user.svg';
import padlockIcon from '../../../assets/img/design/padlock.svg';
import solidIcon from '../../../assets/img/design/Solid.svg';
import maxwhereIcon from '../../../assets/img/design/max.svg';

const DefaultFooter = React.lazy(() => import('../../../containers/DefaultLayout/DefaultFooter'));

class Login extends Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            email: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password } = this.state;
        const { dispatch } = this.props;

        if (email != null && password != null) {
            dispatch(userActions.login(email, password))
            .pipe(
                flatMap(() => rrsmService.login()),
                flatMap(data => rrsmService.start(data.token)),
            )
            .subscribe({
                complete: () => history.push('/projects'),
                error: err => this.growl.show({ severity: 'error', summary: 'Login', detail: err })
            })
        }
    }

    render() {
        const { email, password, submitted } = this.state;
        return (
            <div className="app" style={{ backgroundColor: '#4E6FB5' }}>
                <Growl ref={(el) => this.growl = el} style={{ marginTop: '50px' }} life={4000} />
                <Container fluid className="mt-5" style={{ color: 'white' }}>
                    <Row>
                        <Col xs="6" className="offset-3">
                            <Row className="justify-content-center pt-5 pb-4">
                                <Col xs="12" className="px-5 mx-0">
                                    <img src={roblocklyIcon} className="d-block h-auto w-100 mx-auto" style={{ maxHeight: '600px' }} alt="" />
                                </Col>
                            </Row>
                            <Row className="justify-content-center pt-5 pb-4">
                                <Col xs="12" className="justify-content-center text-left px-5 mx-0">
                                    <span style={{ fontSize: '18px' }} >The Roblockly MaxWhere App is an interactive virtual 3D environment that
                                    provides the popular block-based programming interface for the widely used Universal Robots manipulators.</span>
                                </Col>
                            </Row>
                            <Row className="justify-content-center pt-5">
                                <Col xs="6" className="pl-5">
                                    <img src={solidIcon} className="d-block h-auto w-50 mb-3" style={{ maxHeight: '112px', marginLeft: '20%' }} alt="" />
                                </Col>
                                <Col xs="6" className="pl-5">
                                    <img src={maxwhereIcon} className="d-block h-auto w-50 mt-3" style={{ maxHeight: '90px', marginLeft: '20%' }} alt="" />
                                </Col>
                            </Row>
                            <Row className="justify-content-center pt-5" style={{ marginBottom: '125px' }}>
                                <Col xs="6" className="pl-5">
                                    <span style={{ fontSize: '18px' }} >In Roblockly, anyone (10+) can learn the basics of industrial robot programming with ease.</span>
                                </Col>
                                <Col xs="6" className="pl-5">
                                    <span style={{ fontSize: '18px' }} >Try it out right now! Download MaxWhere and type Roblockly in the search bar.</span>
                                </Col>
                            </Row>
                        </Col>

                        <Col xs="3" className="my-auto">
                            <Form id="loginForm" onSubmit={this.handleSubmit} >
                                <h1>Login</h1>
                                <p>Sign In to your account</p>
                                <FormGroup style={{ paddingRight: '25%' }} >
                                    <InputGroup className="mb-3">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText style={{ backgroundColor: '#34495E', borderColor: '#C4C4C4', borderRightColor: 'transparent' }} >
                                                <img src={userIcon} alt="" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input invalid={!email && submitted} type="email" placeholder="Email" autoComplete="email" name="email" value={email}
                                            onChange={this.handleChange} autoFocus style={{ color: 'white', backgroundColor: '#34495E', borderColor: '#C4C4C4', borderLeftColor: 'transparent', borderRadius: '0 0.25rem 0.25rem 0' }} />
                                        <FormFeedback>Email is required</FormFeedback>
                                    </InputGroup>
                                </FormGroup>

                                <FormGroup style={{ paddingRight: '25%' }} >
                                    <InputGroup className="mb-4">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText style={{ backgroundColor: '#34495E', borderColor: '#C4C4C4', borderRightColor: 'transparent' }} >
                                                <img src={padlockIcon} alt="" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input invalid={!password && submitted} type="password" placeholder="Password" autoComplete="current-password" name="password" value={password}
                                            onChange={this.handleChange} style={{ color: 'white', backgroundColor: '#34495E', borderColor: '#C4C4C4', borderLeftColor: 'transparent', borderRadius: '0 0.25rem 0.25rem 0' }} />
                                        <FormFeedback>Password is required</FormFeedback>
                                    </InputGroup>
                                </FormGroup>

                                <Row className="align-items-center">
                                    <Col xs="12">
                                        <Button className="login-button px-4">Login</Button>
                                        <div className="d-inline-block align-middle ml-3" >
                                            <span>Don't have an account yet?</span>
                                            <br />
                                            <Link to="/register" style={{ color: 'black', fontWeight: 'bold' }}>
                                                <span>Register Now!</span>
                                            </Link>
                                        </div>
                                    </Col>
                                </Row>

                            </Form>
                        </Col>
                    </Row>

                </Container>
                <AppFooter style={{ backgroundColor: '#34495E', borderColor: 'transparent', color: 'white' }} >
                    <DefaultFooter />
                </AppFooter>
            </div>
        );
    }
}


function mapStateToProps(state) {
  const { loggingIn, loggedIn, user } = state.authentication;
  const { alert } = state;
  return {
    loggingIn,
    loggedIn,
    user,
    alert
  };
}

const LoginPage = connect(mapStateToProps)(Login);
export default LoginPage;
