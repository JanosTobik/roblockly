import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import commonPassword from 'common-password';

import { Button, Card, CardBody, CardFooter, Col, Container, Form, FormFeedback, FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Growl } from 'primereact/growl';

import { userActions } from '../../../actions';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      isCommonPassword: false,
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
    // check common passwords
    if (name === 'password') {
      commonPassword(value) ?
        this.setState({ isCommonPassword: true }) :
        this.setState({ isCommonPassword: false })
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { dispatch } = this.props;

    if (email != null && password != null && password.length >= 8) {
      dispatch(userActions.register(email, password))
      .subscribe({
        complete: () => this.growl.show({ severity: 'success', summary: 'Sign up', detail: 'Confirmation link has been sent to your email address!' }),
        error: err => this.growl.show({severity: 'error', summary: 'Sign up', detail: err})
      })
    }

  }

  render() {
    const { email, password, confirmPassword, submitted, isCommonPassword } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <Growl ref={(el) => this.growl = el} style={{ marginTop: '50px' }} life={4000} />
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.handleSubmit}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    
                    <FormGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>@</InputGroupText>
                        </InputGroupAddon>
                        <Input type="email" placeholder="Email" name="email" value={email} onChange={this.handleChange} />
                        <FormFeedback></FormFeedback>
                      </InputGroup>
                    </FormGroup>

                    <FormGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" name="password" value={password} onChange={this.handleChange}
                          invalid={!submitted && (isCommonPassword || (password.length < 8 && password.length > 0))}
                          valid={!submitted && !isCommonPassword && password.length >= 8} />
                          {(!submitted && password.length < 8 && password.length > 0) ?
                            <FormFeedback>Minimum length is 8.</FormFeedback> :
                            <FormFeedback>Your password is too weak!</FormFeedback>
                          }
                        <FormFeedback valid className="help-block">Your password is strong enough</FormFeedback>
                      </InputGroup>
                    </FormGroup>

                    <FormGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input className="col-12" type="password" placeholder="Repeat password" name="confirmPassword" value={confirmPassword} onChange={this.handleChange}
                          invalid={!submitted && confirmPassword !== password && confirmPassword.length > 0} />
                        <FormFeedback>Password does not match</FormFeedback>
                      </InputGroup>
                    </FormGroup>
                    <Button color="success" block disabled={(isCommonPassword || password.length < 8 || !email) ? true : false}>Create Account</Button>
                  </Form>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="12">
                      <span>Already have an account?</span>
                    </Col>
                    <Col xs="12">
                      <Link to="/login">
                        Login
                      </Link>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { registering, registered } = state.registration;
  const {alert } = state;
  return {
      registering,
      registered,
      alert
  };
}

const RegisterPage = connect(mapStateToProps)(Register);
export default RegisterPage;
