import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Button, Col, Container, Row } from 'reactstrap';

import { history } from '../../../helpers';

class ConfirmEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    console.log(this.props);
  }

  onClickRedirect() {
    history.push('/login')
  }

  render() {
    return (
      <React.Fragment>
        {this.props.match.params.success === 'verified' ?
          <div className="app flex-row align-items-center">
            <Container>
              <Row className="justify-content-center">
                <Col md="8">
                  <div className="clearfix d-flex flex-column">
                    <h2 className="align-self-center">Your email has been confirmed</h2>
                    <h4 className="align-self-center text-muted py-3">Thank you for joining Roblockly!</h4>
                    <Button className="mx-auto" color="success" onClick={() => this.onClickRedirect()}>Login</Button>
                  </div>
                </Col>
              </Row>
            </Container>
          </div> :
          <div className="app flex-row align-items-center">
            <Container>
              <Row className="justify-content-center">
                <Col md="10">
                  <div className="clearfix d-flex flex-column">
                    <h3 className="align-self-center">An error occured during your email confirmation</h3>
                    <h4 className="align-self-center text-muted py-3">Please, try again later.</h4>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        }
      </React.Fragment>
    );
  }
}


function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const ConfirmEmailPage = connect(mapStateToProps)(ConfirmEmail);
export default ConfirmEmailPage;
