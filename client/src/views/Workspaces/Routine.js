import React, { Component } from 'react';
import { connect } from 'react-redux';
import { map, flatMap } from 'rxjs/operators';

import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import { Growl } from 'primereact/growl';

import { BlocklyDiv, JoggingOverlay, ToolDialog } from '../../components';
import { routineActions, userActions } from '../../actions';
import { blocklyService } from '../../services';
import { routineToolbox } from '../../misc';

class Routine extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.code = null;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.sid = localStorage.getItem('rrsmSid') ? JSON.parse(localStorage.getItem('rrsmSid')).sid : undefined;
    this.selectedRoutine = null;
    this.joggingOverlay = null;
    this.toolDialog = null;
  }

  componentDidMount() {
    const { dispatch } = this.props;
    this.selectedRoutine = JSON.parse(localStorage.getItem('selectedRoutine'));
    blocklyService.setCurrentProject(this.selectedRoutine);

    // get poses
    dispatch(userActions.getPoses(this.user.id))
    .pipe(
      map(response => {
        let poseList = [];
        response.data.forEach(pose => {
            let block = blocklyService.createNewPoseBlock(pose.poseName, pose.code, pose.poseType);
            poseList.push(block);
        });
        blocklyService.setPoseList(poseList);
      }),
      // get tools
      flatMap(() => dispatch(userActions.getTools(this.user.id))),
      map(response => {
        let toolList = [];
        response.data.forEach(it => {
            // create block with generator
            let block = blocklyService.createNewToolBlock(it.toolName, it.tcpPose, it.payloadMass, it.payloadCog);
            toolList.push(block);
        });
        blocklyService.setToolList(toolList);
      }),
      // inject blockly
      flatMap(() => blocklyService.injectBlockly(routineToolbox)),
      // load workspace
      flatMap(() => dispatch(routineActions.getRoutine(this.selectedRoutine))),
      map(response => {
        blocklyService.loadWorkspace(response.data.definition);
      })
    )
    .subscribe({
      error: () => this.growl.show({ severity: 'error', summary: 'Loading routine', detail: `Loading ${this.selectedRoutine.routineName} failed` })
    })
  }

  onClickClear() {
    blocklyService.clearWorkspace();
  }

  onClickUndo() {
    blocklyService.undoWorkspaceAction();
  }

  onClickRedo() {
    blocklyService.redoWorkspaceAction();
  }

  onClickNewTool() {
    this.toolDialog.setState({
      isDialogVisible: true,
      isNewElement: true
    });
  }

  updateToolList() {
    const { dispatch } = this.props;
    dispatch(userActions.getTools(this.user.id))
    .subscribe(response => {
      let toolList = [];
      response.data.forEach(it => {
        // create block with generator
        let block = blocklyService.createNewToolBlock(it.toolName, it.tcpPose, it.payloadMass, it.payloadCog);
        toolList.push(block);
      });
      blocklyService.setToolList(toolList);
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Growl ref={(el) => this.growl = el} style={{ marginTop: '50px' }} life={4000} />
        <Toolbar >
          <div className="p-toolbar-group-left">
            <Button label="Undo" icon="fa fa-rotate-left" tooltip="Undo workspace" tooltipOptions={{ position: 'right' }} className="warning-button" style={{ marginRight: '.25em' }} onClick={this.onClickUndo.bind(this)} />
            <Button label="Redo" icon="fa fa-rotate-right" iconPos="right" tooltip="Redo workspace" tooltipOptions={{ position: 'right' }} className="warning-button" style={{ marginRight: '.25em' }} onClick={this.onClickRedo.bind(this)} />
            <Button label="Clear" icon="pi pi-times" tooltip="Clear workspace" tooltipOptions={{ position: 'right' }} className="danger-button" style={{ marginRight: '.25em' }} onClick={this.onClickClear.bind(this)} />
            <Button label="New tool" icon="fa fa-wrench" tooltip="Define new tool" tooltipOptions={{ position: 'right' }} className="warning-button" onClick={() => this.onClickNewTool()} />
          </div>
          <div className="p-toolbar-group-right">
            <Button label="Jogging" icon="fa fa-arrows" tooltip="Move the robot on jogging screen" tooltipOptions={{ position: 'left' }} className="common-button" onClick={() => this.joggingOverlay.setState({isDialogVisible: true})} />
          </div>
        </Toolbar>

        <BlocklyDiv />

        <ToolDialog ref={el => this.toolDialog = el} updateTable={() => this.updateToolList()}></ToolDialog>

        <JoggingOverlay ref={el => this.joggingOverlay = el}></JoggingOverlay>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { alert, authentication } = state;
  const { routines } = state.user;
  const { user } = authentication;
  return {
    alert,
    authentication,
    routines,
    user
  };
}

const RoutinePage = connect(mapStateToProps)(Routine);
export default RoutinePage;
