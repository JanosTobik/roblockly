import React, { Component } from 'react';
import { connect } from 'react-redux';
import { map, flatMap } from 'rxjs/operators';

import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import { Growl } from 'primereact/growl';

import { BlocklyDiv, JoggingOverlay, ToolDialog } from '../../components';
import { projectActions, userActions, capsuleActions } from '../../actions';
import { blocklyService } from '../../services';
import { projectToolbox } from '../../misc';

class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.code = null;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.sid = localStorage.getItem('rrsmSid') ? JSON.parse(localStorage.getItem('rrsmSid')).sid : undefined;
    this.selectedProject = null;
    this.joggingOverlay = null;
    this.toolDialog = null;
  }

  componentDidMount() {
    const { dispatch } = this.props;
    this.selectedProject = JSON.parse(localStorage.getItem('selectedProject'));
    blocklyService.setCurrentProject(this.selectedProject);

    // get poses
    dispatch(userActions.getPoses(this.user.id))
    .pipe(
      map(response => {
        let poseList = [];
        response.data.forEach(pose => {
            let block = blocklyService.createNewPoseBlock(pose.poseName, pose.code, pose.poseType);
            poseList.push(block);
        });
        blocklyService.setPoseList(poseList);
      }),
      // get tools
      flatMap(() => dispatch(userActions.getTools(this.user.id))),
      map(response => {
        let toolList = [];
        response.data.forEach(it => {
            // create block with generator
            let block = blocklyService.createNewToolBlock(it.toolName, it.tcpPose, it.payloadMass, it.payloadCog);
            toolList.push(block);
        });
        blocklyService.setToolList(toolList);
      }),
      // get routines
      flatMap(() => dispatch(userActions.getRoutines(this.user.id))),
      map(response => {
        let routineList = [];
        response.data.forEach(it => {
            // get code from block
            let blockCode = blocklyService.convertBlockToCode(it.definition);
            // create block with generator
            let block = blocklyService.createNewRoutineBlock(it.routineName, blockCode);
            routineList.push(block);
        });
        blocklyService.setRoutineList(routineList);
      }),
      // inject blockly
      flatMap(() => blocklyService.injectBlockly(projectToolbox)),
      // load workspace
      flatMap(() => dispatch(projectActions.getProject(this.selectedProject))),
      map(response => {
        blocklyService.loadWorkspace(response.data.definition);
      })
    )
    .subscribe({
      error: () => this.growl.show({ severity: 'error', summary: 'Loading project', detail: `Loading ${this.selectedProject.projectName} failed` })
    })
  }

  onClickPlay() {
    const { dispatch }  = this.props;
    this.code = blocklyService.getWorkspaceCode();
    dispatch(capsuleActions.sendProgram(this.sid, this.code))
    .subscribe({
      next: response => console.log(response),
      error: err => console.log(err)
    });
  }

  onClickStop() {
    const { dispatch }  = this.props;
    this.code = 'stopj(5)\n';
    dispatch(capsuleActions.sendProgram(this.sid, this.code))
    .subscribe({
      next: response => console.log(response),
      error: err => console.log(err)
    });
  }

  onClickClear() {
    blocklyService.clearWorkspace();
  }

  onClickUndo() {
    blocklyService.undoWorkspaceAction();
  }

  onClickRedo() {
    blocklyService.redoWorkspaceAction();
  }

  onClickNewTool() {
    this.toolDialog.setState({
      isDialogVisible: true,
      isNewElement: true
    });
  }

  updateToolList() {
    const { dispatch } = this.props;
    dispatch(userActions.getTools(this.user.id))
    .subscribe(response => {
      let toolList = [];
      response.data.forEach(it => {
        // create block with generator
        let block = blocklyService.createNewToolBlock(it.toolName, it.tcpPose, it.payloadMass, it.payloadCog);
        toolList.push(block);
      });
      blocklyService.setToolList(toolList);
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Growl ref={(el) => this.growl = el} style={{ marginTop: '50px' }} life={4000} />
        <Toolbar >
          <div className="p-toolbar-group-left">
            <Button label="Undo" icon="fa fa-rotate-left" tooltip="Undo workspace" tooltipOptions={{ position: 'right' }} className="warning-button" style={{ marginRight: '.25em' }} onClick={this.onClickUndo.bind(this)} />
            <Button label="Redo" icon="fa fa-rotate-right" iconPos="right" tooltip="Redo workspace" tooltipOptions={{ position: 'right' }} className="warning-button" style={{ marginRight: '.25em' }} onClick={this.onClickRedo.bind(this)} />
            <Button label="Clear" icon="pi pi-times" tooltip="Clear workspace" tooltipOptions={{ position: 'right' }} className="danger-button" style={{ marginRight: '.25em' }} onClick={this.onClickClear.bind(this)} />
            <Button label="New tool" icon="fa fa-wrench" tooltip="Define new tool" tooltipOptions={{ position: 'right' }} className="warning-button" onClick={() => this.onClickNewTool()} />
          </div>
          <div className="p-toolbar-group-right">
            <Button label="Play" icon="fa fa-play" tooltip="Play program" tooltipOptions={{ position: 'left' }} className="success-button" style={{ marginRight: '.25em' }} onClick={this.onClickPlay.bind(this)} />
            <Button label="Stop" icon="fa fa-stop" tooltip="Stop program" tooltipOptions={{ position: 'left' }} className="danger-button" style={{ marginRight: '.25em' }} onClick={() => this.onClickStop()} />
            <Button label="Jogging" icon="fa fa-arrows" tooltip="Move the robot on jogging screen" tooltipOptions={{ position: 'left' }} className="common-button" onClick={() => this.joggingOverlay.setState({isDialogVisible: true})} />
          </div>
        </Toolbar>

        <BlocklyDiv />

        <ToolDialog ref={el => this.toolDialog = el} updateTable={() => this.updateToolList()}></ToolDialog>

        <JoggingOverlay ref={el => this.joggingOverlay = el}></JoggingOverlay>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { alert, authentication } = state;
  const { projects } = state.user;
  const { routines } = state.user;
  const { user } = authentication;
  return {
    alert,
    authentication,
    projects,
    user,
    routines
  };
}

const ProjectPage = connect(mapStateToProps)(Project);
export default ProjectPage;
