import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Button } from 'primereact/button';

import { blocklyService } from '../../services';

import axios from 'axios';


class Jogging extends Component {
    constructor(props) {
      super(props);

      this.state = {}
      this.interval = null;

      this.codeNegX = 'movel(pose_add(get_actual_tool_flange_pose(), p[-0.01, 0, 0, 0, 0, 0]), a=1, v=1)';
      this.codePosX = 'movel(pose_add(get_actual_tool_flange_pose(), p[0.01, 0, 0, 0, 0, 0]), a=1, v=1)';
      this.codeNegY = 'movel(pose_add(get_actual_tool_flange_pose(), p[0, -0.01, 0, 0, 0, 0]), a=1, v=1)';
      this.codePosY = 'movel(pose_add(get_actual_tool_flange_pose(), p[0, 0.01, 0, 0, 0, 0]), a=1, v=1)';
      this.codeNegZ = 'movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, -0.01, 0, 0, 0]), a=1, v=1)';
      this.codePosZ = 'movel(pose_add(get_actual_tool_flange_pose(), p[0, 0, 0.01, 0, 0, 0]), a=1, v=1)';

      // this.onClickSend = this.onClickSend.bind(this);
    }

    componentDidMount() {
      axios.get('/api/robot/connect')
      .then(res => {
        console.log(res.data);
      });
    }

    codeTemplate(code) {
      const functionStart = 'def myFUNC():\n';
      const functionEnd = '\nend';
      return functionStart + code + functionEnd;
    }

    onClickJogging(code)
    {
      blocklyService.sendProgram(this.codeTemplate(code))
      .subscribe({
        next: response => console.log(response),
        error: err => console.log(err)
      });
      this.interval = setInterval(() => {
        console.log('INTERVAL');
        blocklyService.sendProgram(this.codeTemplate(code))
        .subscribe({
          next: response => console.log(response),
          error: err => console.log(err)
        });
      }, 200);
    }

    disconnect()
    {
      clearInterval(this.interval);
    }

    render() {
      return (
        <div className="animated fadeIn">
          <div className="row col-12">
            <Button className="col-1" style={{ marginBottom: '1rem' }} label="-X" onMouseDown={() => this.onClickJogging(this.codeNegX)} onMouseUp={this.disconnect.bind(this)} />
            <div className="col-1"></div>
            <Button className="col-1" style={{ marginBottom: '1rem' }} label="+X" onMouseDown={() => this.onClickJogging(this.codePosX)} onMouseUp={this.disconnect.bind(this)} />
          </div>
          <div className="row col-12">
            <Button className="col-1" style={{ marginBottom: '1rem' }} label="-Y" onMouseDown={() => this.onClickJogging(this.codeNegY)} onMouseUp={this.disconnect.bind(this)} />
            <div className="col-1"></div>
            <Button className="col-1" style={{ marginBottom: '1rem' }} label="+Y" onMouseDown={() => this.onClickJogging(this.codePosY)} onMouseUp={this.disconnect.bind(this)} />
          </div>
          <div className="row col-12">
            <Button className="col-1" style={{ marginBottom: '1rem' }} label="-Z" onMouseDown={() => this.onClickJogging(this.codeNegZ)} onMouseUp={this.disconnect.bind(this)} />
            <div className="col-1"></div>
            <Button className="col-1" style={{ marginBottom: '1rem' }} label="+Z" onMouseDown={() => this.onClickJogging(this.codePosZ)} onMouseUp={this.disconnect.bind(this)} />
          </div>
        </div>

      );
    }
  }

  function mapStateToProps(state) {
    const { alert, authentication, project } = state;
    const { projects } = state.user;
    const { user } = authentication;
    return {
      alert,
      user,
      projects,
      project
    };
  }

  const JoggingPage = connect(mapStateToProps)(Jogging);
  export default JoggingPage;
