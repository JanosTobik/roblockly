import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Row, Col } from 'reactstrap';

import { capsuleActions } from '../../actions';

class RobotState extends Component {
  constructor(props) {
    super(props);

    this.state = {
      urState: {}
    }
  }

  componentDidMount() {
    let rrsmSid = localStorage.getItem('rrsmSid') ? JSON.parse(localStorage.getItem('rrsmSid')).sid : undefined;
    const { dispatch } = this.props;

    dispatch(capsuleActions.getCurrentState(rrsmSid))
    .subscribe(response => { this.setState({ urState: response.data }) });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row className="justify-content-center">
          <Col sm={10}>
            <Row>
              <Col xs><span>X:  </span>{Math.round((this.state.urState.actXXpos * 100000)) / 100}</Col>
              <Col xs><span>Y:  </span>{Math.round((this.state.urState.actYYpos * 100000)) / 100}</Col>
              <Col xs><span>Z:  </span>{Math.round((this.state.urState.actZZpos * 100000)) / 100}</Col>
              <Col xs><span>RX: </span>{Math.round((this.state.urState.actRXpos * 100000)) / 100}</Col>
              <Col xs><span>RY: </span>{Math.round((this.state.urState.actRYpos * 100000)) / 100}</Col>
              <Col xs><span>RZ: </span>{Math.round((this.state.urState.actRZpos * 100000)) / 100}</Col>
            </Row>
            <Row>
              <Col xs><span>J1: </span>{Math.round((this.state.urState.actJ1pos * (180 / Math.PI)) * 100) / 100}</Col>
              <Col xs><span>J2: </span>{Math.round((this.state.urState.actJ2pos * (180 / Math.PI)) * 100) / 100}</Col>
              <Col xs><span>J3: </span>{Math.round((this.state.urState.actJ3pos * (180 / Math.PI)) * 100) / 100}</Col>
              <Col xs><span>J4: </span>{Math.round((this.state.urState.actJ4pos * (180 / Math.PI)) * 100) / 100}</Col>
              <Col xs><span>J5: </span>{Math.round((this.state.urState.actJ5pos * (180 / Math.PI)) * 100) / 100}</Col>
              <Col xs><span>J6: </span>{Math.round((this.state.urState.actJ6pos * (180 / Math.PI)) * 100) / 100}</Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert, authentication, capsule } = state;
  const { user } = authentication;
  return {
    alert,
    user,
    capsule
  };
}

const RobotStatePage = connect(mapStateToProps)(RobotState);
export default RobotStatePage;
