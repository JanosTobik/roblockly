import React, { Component } from 'react';

import { connect } from 'react-redux';
import commonPassword from 'common-password';

import { Card, CardBody, CardHeader, Col, ListGroup, ListGroupItem, Row, TabContent, TabPane, Form, FormGroup, FormFeedback, Label, Input, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Growl } from 'primereact/growl';

import { history } from '../../helpers';
import { userActions } from '../../actions';

class Settings extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 0,
      oldPassword: '',
      newPassword: '',
      confirmNewPassword: '',
      isCommonPassword: false,
      submitted: false,
      isDialogVisible: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.onSubmitUpdatePassword = this.onSubmitUpdatePassword.bind(this);
    this.onSubmitDeleteAccount = this.onSubmitDeleteAccount.bind(this);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
    // check common passwords
    if (name === 'newPassword') {
      commonPassword(value) ?
        this.setState({ isCommonPassword: true }) :
        this.setState({ isCommonPassword: false })
    }
  }

  onSubmitUpdatePassword(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { oldPassword, newPassword, confirmNewPassword } = this.state;
    const { dispatch, user } = this.props;

    if (oldPassword != null && newPassword != null && newPassword.length >= 8 && newPassword === confirmNewPassword) {
      dispatch(userActions.updatePassword(user.id, oldPassword, newPassword))
      .subscribe({
        next: response => this.growl.show({severity: 'success', summary: 'Update password', detail: response.message}),
        error: err => this.growl.show({severity: 'error', summary: 'Update password', detail: err}),
        complete: () => this.setState({oldPassword: '', newPassword: '', confirmNewPassword: ''})
      })
    }
  }

  onSubmitDeleteAccount(e) {
    e.preventDefault();

    this.setState({ isDialogVisible: true });
  }

  deleteAccount() {
    const { dispatch, user } = this.props;
    dispatch(userActions.deleteAccount(user.id))
    .subscribe({
      error: err => this.growl.show({severity: 'error', summary: 'Delete account', detail: err}),
      complete: () => history.push('/login')
    })

    this.setState({ isDialogVisible: false });
  }

  render() {
    const { oldPassword, newPassword, confirmNewPassword, isCommonPassword, submitted } = this.state;
    return (
      <div className="animated fadeIn">
        <Growl ref={(el) => this.growl = el} style={{ marginTop: '50px' }} life={4000} />
        <Row className="justify-content-center">
          <Col sm={10}>
            <Card>
              <CardHeader>
                <i className="icon-settings"></i><strong>Settings</strong> <small></small>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col sm="3">
                    <ListGroup id="list-tab" role="tablist">
                      <ListGroupItem className="bg-light"><strong>Personal settings</strong></ListGroupItem>
                      <ListGroupItem onClick={() => this.toggle(0)} action active={this.state.activeTab === 0} >Account</ListGroupItem>
                      {/* <ListGroupItem onClick={() => this.toggle(1)} action active={this.state.activeTab === 1} >Settings 2</ListGroupItem>
                      <ListGroupItem onClick={() => this.toggle(2)} action active={this.state.activeTab === 2} >Settings 3</ListGroupItem>
                      <ListGroupItem onClick={() => this.toggle(3)} action active={this.state.activeTab === 3} >Settings 4</ListGroupItem> */}
                    </ListGroup>
                  </Col>
                  <Col sm="9">
                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId={0} >
                        <Form onSubmit={this.onSubmitUpdatePassword}>
                          <p className="h4"><u>Change password</u></p>
                          <br />
                          <FormGroup>
                            <Label htmlFor="oldPassword">Old password</Label>
                            <Col xs="6" className="px-0">
                              <Input type="password" id="oldPassword" name="oldPassword" value={oldPassword} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup>
                            <Label htmlFor="newPassword">New password</Label>
                            <Col xs="6" className="px-0">
                              <Input type="password" id="newPassword" name="newPassword" value={newPassword} onChange={this.handleChange}
                                invalid={!submitted && (isCommonPassword || (newPassword.length < 8 && newPassword.length > 0))}
                                valid={!submitted && !isCommonPassword && newPassword.length >= 8} />
                              {(!submitted && newPassword.length < 8 && newPassword.length > 0) ?
                                <FormFeedback>Minimum length is 8.</FormFeedback> :
                                <FormFeedback>Your password is too weak!</FormFeedback>
                              }
                              <FormFeedback valid className="help-block">Your password is strong enough</FormFeedback>
                            </Col>
                          </FormGroup>
                          <FormGroup>
                            <Label htmlFor="confirmNewPassword">Confirm new password</Label>
                            <Col xs="6" className="px-0">
                              <Input type="password" id="confirmNewPassword" name="confirmNewPassword" value={confirmNewPassword} onChange={this.handleChange}
                                invalid={!submitted && confirmNewPassword !== newPassword && confirmNewPassword.length > 0} />
                              <FormFeedback>Password does not match</FormFeedback>
                            </Col>
                          </FormGroup>
                          <FormGroup className="form-actions">
                            <Button type="submit" color="primary" disabled={(isCommonPassword || newPassword.length < 8 || !oldPassword || !confirmNewPassword) ? true : false}>Update password</Button>
                          </FormGroup>
                        </Form>

                        <Form onSubmit={this.onSubmitDeleteAccount}>
                          <br />
                          <p className="h4 text-danger"><u>Delete account</u></p>
                          <br />
                          <span>Once you delete your account, there is no going back. Please be certain.</span>
                          <FormGroup className="form-actions">
                            <Button type="submit" color="danger">Delete your account</Button>
                          </FormGroup>
                        </Form>

                        <Modal isOpen={this.state.isDialogVisible} >
                          <ModalHeader>
                            <span>Delete account</span>
                          </ModalHeader>
                          <ModalBody>
                            Once you delete your account, there is no going back. Please be certain.
                          </ModalBody>
                          <ModalFooter>
                            <Button color="danger" onClick={this.deleteAccount.bind(this)} >Submit</Button>
                            <Button color="light" onClick={() => this.setState({ isDialogVisible: false })} >Cancel</Button>
                          </ModalFooter>
                        </Modal>
                      </TabPane>
                      <TabPane tabId={1}>
                        <p>Cupidatat quis ad sint excepteur laborum in esse qui. Et excepteur consectetur ex nisi eu do cillum ad laborum. Mollit et eu officia
                          dolore sunt Lorem culpa qui commodo velit ex amet id ex. Officia anim incididunt laboris deserunt
                          anim aute dolor incididunt veniam aute dolore do exercitation. Dolor nisi culpa ex ad irure in elit eu dolore. Ad laboris ipsum
                          reprehenderit irure non commodo enim culpa commodo veniam incididunt veniam ad.</p>
                      </TabPane>
                      <TabPane tabId={2}>
                        <p>Ut ut do pariatur aliquip aliqua aliquip exercitation do nostrud commodo reprehenderit aute ipsum voluptate. Irure Lorem et laboris
                          nostrud amet cupidatat cupidatat anim do ut velit mollit consequat enim tempor. Consectetur
                          est minim nostrud nostrud consectetur irure labore voluptate irure. Ipsum id Lorem sit sint voluptate est pariatur eu ad cupidatat et
                          deserunt culpa sit eiusmod deserunt. Consectetur et fugiat anim do eiusmod aliquip nulla
                          laborum elit adipisicing pariatur cillum.</p>
                      </TabPane>
                      <TabPane tabId={3}>
                        <p>Irure enim occaecat labore sit qui aliquip reprehenderit amet velit. Deserunt ullamco ex elit nostrud ut dolore nisi officia magna
                          sit occaecat laboris sunt dolor. Nisi eu minim cillum occaecat aute est cupidatat aliqua labore
                          aute occaecat ea aliquip sunt amet. Aute mollit dolor ut exercitation irure commodo non amet consectetur quis amet culpa. Quis ullamco
                          nisi amet qui aute irure eu. Magna labore dolor quis ex labore id nostrud deserunt dolor
                          eiusmod eu pariatur culpa mollit in irure.</p>
                      </TabPane>
                    </TabContent>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { user } = state.authentication;
  const { alert } = state;
  return {
    alert,
    user
  };
}

const SettingsPage = connect(mapStateToProps)(Settings);
export default SettingsPage;
