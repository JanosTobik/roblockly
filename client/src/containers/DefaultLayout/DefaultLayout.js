import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';

import {
    AppFooter,
    AppHeader,
} from '@coreui/react';
// routes config
import routes from '../../routes';

import { userActions } from '../../actions';

import backgroundIcon from '../../assets/img/design/02_roblockly_logo_full_white.svg';

const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {

    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    signOut() {
        this.props.dispatch(userActions.logout());
        this.props.history.push('/login')
    }

    render() {
        if (JSON.parse(localStorage.getItem('rrsmSid'))) {
            var sid = JSON.parse(localStorage.getItem('rrsmSid')).sid;
        }
        return (
            <div className="app">
                <AppHeader fixed>
                    <Suspense fallback={this.loading()}>
                        <DefaultHeader onLogout={() => this.signOut()} />
                    </Suspense>
                </AppHeader>
                <div className="app-body">
                    <main className="main" style={{
                        backgroundImage: `url(${backgroundIcon})`,
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'bottom'
                    }}>
                        <Container fluid>
                            <Suspense fallback={this.loading()}>
                                <Switch>
                                    {routes.map((route, idx) => {
                                        return route.component ? (
                                            <Route
                                                key={idx}
                                                path={route.path}
                                                exact={route.exact}
                                                name={route.name}
                                                render={props => (
                                                    <route.component {...props} />
                                                )} />
                                        ) : (null);
                                    })}
                                    <Redirect from="/" to={`/${sid}/projects`} />
                                </Switch>
                            </Suspense>
                        </Container>
                    </main>
                </div>
                <AppFooter fixed>
                    <Suspense fallback={this.loading()}>
                        <DefaultFooter />
                    </Suspense>
                </AppFooter>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {};
}
const DefaultLayoutObject = connect(mapStateToProps)(DefaultLayout);
export default DefaultLayoutObject;
