import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { Timer } from '../../components';

import { AppNavbarBrand } from '@coreui/react';

import roblocklyIcon from '../../assets/img/design/01_roblockly_logo.svg';
import projectsIcon from '../../assets/img/design/start-up.svg';
import routinesIcon1 from '../../assets/img/design/riddle.svg';
import routinesIcon2 from '../../assets/img/design/riddle-1.svg';
import posesIcon from '../../assets/img/design/kar.svg';
import toolsIcon from '../../assets/img/design/handyman-tools.svg';
import settingsIcon from '../../assets/img/design/settings.svg';

const propTypes = {
    children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
    render() {

        // eslint-disable-next-line
        const { children, ...attributes } = this.props;
        var sid = localStorage.getItem('rrsmSid') ? JSON.parse(localStorage.getItem('rrsmSid')).sid : undefined;
        return (
            <React.Fragment>
                <AppNavbarBrand
                    full={{ src: roblocklyIcon, width: 100, height: 40, alt: 'Roblockly Logo' }}
                />
                <Nav className="d-md-down-none" navbar>
                    <div className="px-5"></div>
                    <NavItem className="pr-3">
                        <Link to={`/${sid}/projects`}>
                            <img src={projectsIcon} style={{ height: "17px", width: "17px", marginRight: '5px' }} alt="" />
                            <span className="navItem align-middle">PROJECTS</span>
                        </Link>
                    </NavItem>
                    <NavItem className="pr-3">
                        <Link to={`/${sid}/routines`}>
                            <img src={routinesIcon1} style={{ height: "17px", width: "17px", marginRight: '-5px' }} alt="" />
                            <img src={routinesIcon2} style={{ height: "17px", width: "17px", marginRight: '5px' }} alt="" />
                            <span className="navItem align-middle">ROUTINES</span>
                        </Link>
                    </NavItem>
                    <NavItem className="pr-3">
                        <Link to={`/${sid}/poses`}>
                            <img src={posesIcon} style={{ height: "20px", width: "20px", marginRight: '5px', marginBottom: '1px' }} alt="" />
                            <span className="navItem align-middle">POSES</span>
                        </Link>
                    </NavItem>
                    <NavItem className="pr-3">
                        <Link to={`/${sid}/tools`}>
                            <img src={toolsIcon} style={{ height: "16px", width: "16px", marginRight: '5px' }} alt="" />
                            <span className="navItem align-middle">TOOLS</span>
                        </Link>
                    </NavItem>
                    <NavItem className="pr-3">
                        <Link to={`/${sid}/settings`}>
                            <img src={settingsIcon} style={{ height: "20px", width: "20px", marginRight: '5px', marginBottom: '1px' }} alt="" />
                            <span className="navItem align-middle">SETTINGS</span>
                        </Link>
                    </NavItem>
                </Nav>

                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <Timer onLogout={() => this.props.onLogout()}></Timer>
                    </NavItem>
                    <UncontrolledDropdown nav direction="down">
                        <DropdownToggle nav>
                            <i className="fa fa-user-circle fa-lg"></i>
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
                            <Link to={`/${sid}/projects`}><DropdownItem><i className="fa fa-list-ul"></i> Projects</DropdownItem></Link>
                            <Link to={`/${sid}/routines`}><DropdownItem><i className="fa fa-list-ul"></i> Routines</DropdownItem></Link>
                            <Link to={`/${sid}/poses`}><DropdownItem><i className="fa fa-list-ul"></i> Poses</DropdownItem></Link>
                            <Link to={`/${sid}/tools`}><DropdownItem><i className="fa fa-list-ul"></i> Tools</DropdownItem></Link>
                            <Link to={`/${sid}/settings`}><DropdownItem><i className="icon-settings"></i> Settings</DropdownItem></Link>
                            <DropdownItem onClick={() => this.props.onLogout()}><i className="fa fa-sign-out"></i> Logout</DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </React.Fragment>
        );
    }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
