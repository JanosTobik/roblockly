import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Button } from 'primereact/button';
import { Growl } from 'primereact/growl';

import { Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { projectActions } from '../../../actions';

class ProjectDeleteDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDialogVisible: false,
            project: {},
        };

        this.user = JSON.parse(localStorage.getItem('user'));
    }

    onClickDelete() {
        const { dispatch } = this.props;
        const { projectId } = this.state.project;

        dispatch(projectActions.deleteProject(projectId))
        .subscribe(response => this.growl.show({ severity: 'success', summary: 'Delete project', detail: response.message }));

        if(this.props.updateTable) {
            this.props.updateTable();
        }

        this.setState({
            project: {},
            isDialogVisible: false
        });
    }

    render() {
        return (
            <React.Fragment>
                <Growl ref={(el) => this.growl = el} style={{marginTop: '50px'}} life={3000} />
                <Modal isOpen={this.state.isDialogVisible}>
                    <ModalHeader>
                        <span>Delete project</span>
                    </ModalHeader>
                    <ModalBody>
                        <div style={{ paddingTop: '3em', paddingBottom: '3em', paddingLeft: '0.5em' }}>
                            {this.state.project.projectName}<span> will be deleted. This action cannot be undone!</span>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="common-button" label="Confirm" icon="pi pi-check" onClick={() => this.onClickDelete()} />
                        <Button className="common-button" label="Cancel" icon="pi pi-times" onClick={() => this.setState({ isDialogVisible: false})} />
                    </ModalFooter>

                </Modal>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    const { alert, authentication } = state;
    const { user } = authentication;
    return {
        alert,
        authentication,
        user
    };
}

const ProjectDeleteDialogComp = connect(mapStateToProps, null, null, { forwardRef: true })(ProjectDeleteDialog);
export { ProjectDeleteDialogComp as ProjectDeleteDialog };
