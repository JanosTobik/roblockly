import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tap } from 'rxjs/operators';

import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Growl } from 'primereact/growl';

import { Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { routineActions } from '../../../actions';

class RoutineDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDialogVisible: false,
            isNewElement: false,
            routine: {
                routineName: '',
                description: ''
            },
        };

        this.user = JSON.parse(localStorage.getItem('user'));
    }

    updateProperty(property, value) {
        let routine = this.state.routine;
        routine[property] = value;
        this.setState({ routine: routine });
    }

    onClickSave() {
        const { dispatch } = this.props;
        const { isNewElement } = this.state;
        const { routineId, routineName, description, definition } = this.state.routine;

        let observable$;

        if (isNewElement) {
            observable$ = dispatch(routineActions.addNew(this.user.id, routineName, description))
            .pipe(
                tap({
                    next: response => this.growl.show({ severity: 'success', summary: 'New routine', detail: response.message }),
                    error: err => this.growl.show({ severity: 'error', summary: 'New routine', detail: err })
                })
            );
        } else {
            observable$ = dispatch(routineActions.update({ routineId, routineName, description, definition }))
            .pipe(
                tap({
                    next: response => this.growl.show({ severity: 'success', summary: 'Edit routine', detail: response.message }),
                    error: err => this.growl.show({ severity: 'error', summary: 'Edit routine', detail: err })
                })
            );
        }

        observable$.subscribe({});

        if(this.props.updateTable) {
            this.props.updateTable();
        }

        this.setState({
            routine: {
                routineName: '',
                description: ''
            },
            isDialogVisible: false
        })
    }

    render() {
        return (
            <React.Fragment>
                <Growl ref={(el) => this.growl = el} style={{marginTop: '50px'}} life={3000} />
                <Modal isOpen={this.state.isDialogVisible}>
                    <ModalHeader>
                        <span>Routine Details</span>
                    </ModalHeader>
                    <ModalBody>
                        <div className="row justify-content-center">
                            <div className="col-11" style={{ padding: '.75em' }}>
                                <label>Routine name</label>
                            </div>
                            <div className="col-11" style={{ padding: '.5em' }}>
                                <InputText className="col-12" id="routineName" onChange={(e) => { this.updateProperty('routineName', e.target.value) }} value={this.state.routine.routineName} />
                            </div>
                            <div className="col-11" style={{ padding: '.75em' }}>
                                <label>Routine description</label>
                            </div>
                            <div className="col-11" style={{ padding: '.5em' }}>
                                <InputTextarea className="col-12" id="description" rows={5} cols={30} onChange={(e) => { this.updateProperty('description', e.target.value) }} value={this.state.routine.description} />
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="common-button" label="Save" icon="pi pi-check" onClick={() => this.onClickSave()} />
                        <Button className="common-button" label="Cancel" icon="pi pi-times" onClick={() => this.setState({ isDialogVisible: false})} />
                    </ModalFooter>

                </Modal>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    const { alert, authentication } = state;
    const { user } = authentication;
    return {
        alert,
        authentication,
        user
    };
}

const RoutineDialogComp = connect(mapStateToProps, null, null, { forwardRef: true })(RoutineDialog);
export { RoutineDialogComp as RoutineDialog };
