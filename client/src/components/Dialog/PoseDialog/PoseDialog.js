import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tap } from 'rxjs/operators';

import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Growl } from 'primereact/growl';

import { Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { poseActions } from '../../../actions';

class PoseDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDialogVisible: false,
            isNewElement: false,
            pose: {
                poseName: '',
                description: ''
            },
        };

        this.user = JSON.parse(localStorage.getItem('user'));
    }

    updateProperty(property, value) {
        let pose = this.state.pose;
        pose[property] = value;
        this.setState({ pose: pose });
    }

    onClickSave() {
        const { dispatch } = this.props;
        const { isNewElement } = this.state;
        const { poseId, poseName, description, definition } = this.state.pose;

        let observable$;

        if (isNewElement) {
            observable$ = dispatch(poseActions.addNew(this.user.id, poseName, description))
            .pipe(
                tap({
                    next: response => this.growl.show({ severity: 'success', summary: 'New pose', detail: response.message }),
                    error: err => this.growl.show({ severity: 'error', summary: 'New pose', detail: err })
                })
            );
        } else {
            observable$ = dispatch(poseActions.update({ poseId, poseName, description, definition }))
            .pipe(
                tap({
                    next: response => this.growl.show({ severity: 'success', summary: 'Edit pose', detail: response.message }),
                    error: err => this.growl.show({ severity: 'error', summary: 'Edit pose', detail: err })
                })
            );
        }

        observable$.subscribe({});

        if(this.props.updateTable) {
            this.props.updateTable();
        }

        this.setState({
            pose: {
                poseName: '',
                description: ''
            },
            isDialogVisible: false
        })
    }

    render() {
        return (
            <React.Fragment>
                <Growl ref={(el) => this.growl = el} style={{marginTop: '50px'}} life={3000} />
                <Modal isOpen={this.state.isDialogVisible}>
                    <ModalHeader>
                        <span>Pose Details</span>
                    </ModalHeader>
                    <ModalBody>
                        <div className="row justify-content-center">
                            <div className="col-11" style={{ padding: '.75em' }}>
                                <label>Pose name</label>
                            </div>
                            <div className="col-11" style={{ padding: '.5em' }}>
                                <InputText className="col-12" id="poseName" onChange={(e) => { this.updateProperty('poseName', e.target.value) }} value={this.state.pose.poseName} />
                            </div>
                            <div className="col-11" style={{ padding: '.75em' }}>
                                <label>Pose description</label>
                            </div>
                            <div className="col-11" style={{ padding: '.5em' }}>
                                <InputTextarea className="col-12" id="description" rows={5} cols={30} onChange={(e) => { this.updateProperty('description', e.target.value) }} value={this.state.pose.description} />
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="common-button" label="Save" icon="pi pi-check" onClick={() => this.onClickSave()} />
                        <Button className="common-button" label="Cancel" icon="pi pi-times" onClick={() => this.setState({ isDialogVisible: false})} />
                    </ModalFooter>

                </Modal>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    const { alert, authentication } = state;
    const { user } = authentication;
    return {
        alert,
        authentication,
        user
    };
}

const PoseDialogComp = connect(mapStateToProps, null, null, { forwardRef: true })(PoseDialog);
export { PoseDialogComp as PoseDialog };
