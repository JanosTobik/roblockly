import React from 'react';

const Projects = React.lazy(() => import('./views/Dashboard/Projects'));
const Routines = React.lazy(() => import('./views/Dashboard/Routines'));
const Poses = React.lazy(() => import('./views/Dashboard/Poses'));
const Tools = React.lazy(() => import('./views/Dashboard/Tools'));

const Project = React.lazy(() => import('./views/Workspaces/Project'));
const Routine = React.lazy(() => import('./views/Workspaces/Routine'));

const Settings = React.lazy(() => import('./views/Settings/Settings'));

const Jogging = React.lazy(() => import('./views/Test/Jogging'));
const RobotState = React.lazy(() => import('./views/Test/RobotState'));


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/:sid/projects', exact: true, name: 'Projects', component: Projects },
  { path: '/:sid/routines', exact: true, name: 'Routines', component: Routines },
  { path: '/:sid/poses', exact: true, name: 'Poses', component: Poses },
  { path: '/:sid/tools', exact: true, name: 'Tools', component: Tools },
  { path: '/:sid/projects/:projectName', exact: true, name: 'Project', component: Project },
  { path: '/:sid/routines/:routineName', exact: true, name: 'Routine', component: Routine },
  { path: '/:sid/settings', name: 'Settings', component: Settings },
  { path: '/:sid/jogging', name: 'Jogging', component: Jogging },
  { path: '/:sid/robotstate', name: 'RobotState', component: RobotState },
];

export default routes;
