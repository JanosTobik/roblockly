export default {
  items: [
    {
      name: 'Projects',
      url: '/projects',
      icon: 'fa fa-table'
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'icon-settings'
    },    
  ],
};
