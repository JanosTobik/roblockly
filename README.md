## A Roblockly elérhető a roblockly.com oldalon.
## A legújabb verzió forráskódja a bitbucket.org/JanosTobik/roblockly repository-ban található.
---

## Node verzió

    12.x

## Lokális indítás
    Lokális környezetben a robot vezérléséhez URSim használata szükséges egy virtuális gépen.
    A URSim letölthető a Universal Robots hivatalos weboldaláról: https://www.universal-robots.com/download/
    A webszerveren az összes TCP alapú kommunikációt a virtuális gép ip címére kell konfigurálni.
    Éles helyzetben a szimulációs kapszulákra van konfigurálva.
    Érintett fájlok:
        /server/middlewares/capsuleHandler.js
        /server/utils/WebsocketConnector.js
    Példák:
        client.connect(30002, sid); => client.connect(30002, '192.168.1.11');
        new URStateReceiver(30003, sid); => new URStateReceiver(30003, '192.168.1.11');

### Webszerver

    npm install
    npm run devserver

### Kliens

    npm install
    npm run build

    # A kliens a /client/src/environment/environment.js fájlból szedi a webszerver URL címét.
    # Production környezetben az éles rendszer url címe van megadva, egyéb esetben http://localhost:8080.
    # Szükség esetén módosítani kell.
    # A kliens generált fájljait a webszerver hosztolja.
    # Egyéb esetben elindítható a React Developer App az npm install után.

React Developer App indítása (alapértelmezetten 3000-es porton):
    
    npm start

## Környezeti változók

    A környezeti változókat .env fájlba vagy Dockerfile-ba kell felvenni.

### Webszerver

    # application
    NODE_ENV=                   # production or develop
    URL=                        # server url - http://{ip-address/domain-name}:{port}
    PORT=                       # server port
    TOKEN_SECRET=               # jwt token secret
    SQL_LOGGING=                # true or false
    HTTP_LOGGING=               # true or false

    # sqlite database
    DB_PATH=                    # sqlite database path

    # email
    SMTP_SERVER=                # smtp server name
    SMTP_PORT=                  # smtp server port
    SMTP_SECURED=               # true for port 465, false for other ports
    EMAIL_USER=                 # email address
    EMAIL_PASS=                 # email password

    # RRSM
    RRSM_URL=                   # rrsm url

### Kliens

    PORT=                       # development application port
    CHOKIDAR_USEPOLLING=        # true or false

    BROWSER=                    # none - do not open browser on startup

    REACT_APP_ENV=              # production or develop

## REST interfész

### Authentikáció

Funkció | URI | HTTP Metódus | Státusz
:---:|:---:|:---:|:---:|:---:
Bejelentkezés | */api/login* | POST | 200, 404
Regisztráció | */api/signup* | POST | 201, 400, 500
Regisztráció megerősítése | */api/confirm/:key* | GET | 301

---

### Felhasználói funkciók
Funkció | URI | HTTP Metódus | Státusz
:---:|:---:|:---:|:---:|:---:
Projektek lekérdezése | */api/users/:id/projects* | GET | 200, 404
Alprojektek lekérdezése | */api/users/:id/subprocesses* | GET | 200, 404
Pozíciók lekérdezése | */api/users/:id/poses* | GET | 200, 404
Jelszó változtatás | */api/users/:id/password* | PUT | 200, 500
Felhasználó törlése | */api/users/:id* | DELETE | 200, 500

---

### Projekt funkciók
Funkció | URI | HTTP Metódus | Státusz
:---:|:---:|:---:|:---:|:---:
Projekt lekérdezése | */api/projects/:id* | GET | 200, 404
Projekt létrehozása | */api/project* | POST | 201, 404, 500
Projekt módosítása | */api/project* | PUT | 200, 500
Projekt törlése | */api/projects/:id* | DELETE | 200, 500

---

### Alprojekt funkciók
Funkció | URI | HTTP Metódus | Státusz
:---:|:---:|:---:|:---:|:---:
Alprojekt lekérdezése | */api/routines/:id* | GET | 200, 404
Alprojekt létrehozása | */api/routine* | POST | 201, 404, 500
Alprojekt módosítása | */api/routine* | PUT | 200, 500
Alprojekt törlése | */api/routines/:id* | DELETE | 200, 500

---

### Pozíció funkciók
Funkció | URI | HTTP Metódus | Státusz
:---:|:---:|:---:|:---:|:---:
Pozíció lekérdezése | */api/poses/:id* | GET | 200, 404
Pozíció létrehozása | */api/pose* | POST | 201, 404, 500
Pozíció módosítása | */api/pose* | PUT | 200, 500
Pozíció törlése | */api/poses/:id* | DELETE | 200, 500

---

### Capsule funkciók
Funkció | URI | HTTP Metódus | Státusz
:---:|:---:|:---:|:---:|:---:
Program küldése | */api/capsule/:sid* | POST | 200, 500
Robot állapotának lekérdezése | */api/capsule/state/:sid* | GET | 200, 500
